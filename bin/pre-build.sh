#!/bin/bash

# Check for environment variables or fall back to default values
[ -z ${PREBUILD_DEBUG+x} ] && export PREBUILD_DEBUG=0
[ -z ${PREBUILD_CONTENT+x} ] && export PREBUILD_CONTENT="content"
[ -z ${PREBUILD_STATIC+x} ] && export PREBUILD_STATIC="static"
[ -z ${PREBUILD_DATA+x} ] && export PREBUILD_DATA="data"

function run {
    case "$1" in
        "uncommitted")
            # content/ is always tracked so no need to check untracked files
            FILES=$(git diff --name-only "$PREBUILD_CONTENT"/)
            # Only committed stuff should get check for deletion, otherwise it's gonna
            # be super hard knowing if the content is inserted back
            CHANGES=$(git diff "$PREBUILD_CONTENT"/)
            ;;
        "committed")
            FILES=$(git diff --name-only origin/master HEAD "$PREBUILD_CONTENT"/)
            CHANGES=$(git diff origin/master HEAD "$PREBUILD_CONTENT"/)
            ;;
        "init")
            
            ;;
    esac

    for fileName in ./bin/hooks/pre-build/*.sh; do
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: Running plugin $fileName"
        # Sometimes the loop gives a file that does not exist (eg. when no match found)
        [ -e "$fileName" ] || continue

        request=$($fileName)
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: Registering regex $request for plugin $fileName."
    
        if [ -z ${FILES+x} ]; then
            [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: Initializing plugin $fileName: looking for regex throughout content."
            MATCHINGFILES=$(grep -rPz "$request" "$PREBUILD_CONTENT"/)
        elif [[ $FILES != "" ]]; then
            MATCHINGFILES=()
            # TODO: Run one grep for all the files and extract matching filenames from there
            for FILE in $FILES; do
                grep -Pqs "$request" "$FILE"
                if [[ $? = 0 ]]; then
                    [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: File $FILE matches regex."
                    MATCHINGFILES+=("$FILE")
                else 
                    [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: File $FILE does not match regex!"
                    continue
                fi
            done
        else
            MATCHINGFILES=()
        fi
        
        if [ ! -z ${CHANGES+x} ]; then
            # We have some changes, let's filter them with the regex
            MATCHINGCHANGES=$(echo "$CHANGES" | grep -P "$request") 
            [[ $? != 0 ]] && unset MATCHINGCHANGES && [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: No changes found for $fileName with regex $request"
        fi
        
        # We don't need to check for empty CHANGES as we unset it properly
        if [ -z ${MATCHINGCHANGES+x} ]; then
            # Skip plugin if no file matches, and no changes match
            [ ${#MATCHINGFILES[@]} -eq 0 ] && continue
            # Only files should be passed
            $fileName run "${MATCHINGFILES[@]}"
            [[ $? != 0 ]] && echo "[pre-build.sh] ERROR: Running plugin $fileName failed." && return 1
        else
            if [ ${#MATCHINGFILES[@]} -gt 0 ]; then
                echo "$MATCHINGCHANGES" | $fileName run "${MATCHINGFILES[@]}"
                [[ $? != 0 ]] && echo "[pre-build.sh] ERROR: Running plugin $fileName failed." && return 1
            else
                echo "$MATCHINGCHANGES" | $fileName run
                [[ $? != 0 ]] && echo "[pre-build.sh] ERROR: Running plugin $fileName failed." && return 1
            fi
        fi
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[pre-build.sh] DEBUG: Done running plugin $fileName."
    done
    
    return 0
}

function help {
    echo "hello!"
    exit
}

if [[ $# = 0 ]]; then
    help
fi

case "$1" in
    "help")
        help
        ;;
    *)
        run $1
        [[ $? != 0 ]] && echo "[pre-build.sh] ERROR: Failed because one or more plugins encountered errors." && exit 1
        ;;
esac

exit 0
