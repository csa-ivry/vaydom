#!/bin/bash 

function tidyFile {
    tidy -f /dev/null -config ~/tidy.conf -m $1
}

# We need to export tidyFile because it's called from find and not from this script
export -f tidyFile

function help {
    echo "build.sh -- build & deploy"
    echo "----"
    echo "build.sh COMMAND"
    echo "         verbose for hugo --verbose"
    echo "         debug for hugo --debug"
    echo "         perf for hugo --templateMetrics --templateMetricsHints"
    echo "If tidy is installed, your HTML files will be tidied following the config in your ~/tidy.conf."
}

MYDIR=$(pwd)
cd /home/user/vaydom

# Check that nothing is in staging area

if [[ $(git diff --cached content/*) != "" ]]; then
    echo "[build.sh] Cannot build while stage content has not been committed."
    echo "           Please either commit or remove your changes with git checkout -- FILES"
    exit 1
fi

# Set env variables for the script
export PREBUILD_DEBUG=0

# Start pre-build scripts
./bin/pre-build.sh uncommitted
[[ $? != 0 ]] && echo "✘ Failed to run content plugins" && exit 1
./bin/pre-build.sh committed
[[ $? != 0 ]] && echo "✘ Failed to run content plugins" && exit 1
#./bin/pre-build.sh init


# Start build procedure

rm -rf public/*

echo 'baseURL: "https://vaydom.cecmu.org/"' > config.yaml
cat config.template.yaml >> config.yaml

if [[ $# = 1 ]]; then
    case "$1" in
    "help")
        help
        ;;
    "verbose")
        hugo --verbose --i18n-warnings --ignoreCache --config config.yaml,menus.yaml
        ;;
    "debug")
        hugo --debug --i18n-warnings --ignoreCache --config config.yaml,menus.yaml
        ;;
    "perf")
            hugo --templateMetrics --templateMetricsHints --config config.yaml,menus.yaml
        ;;
    esac
else
    hugo --i18n-warnings --ignoreCache --config config.yaml,menus.yaml
fi

if [[ $? != 0 ]]; then
    echo "✘ Hugo build process failed."
    exit 1
fi
echo "✔ Hugo build process succeeded."

if command -v tidy > /dev/null; then
    echo "Sending all HTML files to be tidied."
    find public -name '*.html' -exec bash -c 'tidyFile {}' \;
    if [[ $? != 0 ]]; then
        echo "✘ Tidy process failed."
        exit 1
    fi
else
    echo "✚ Tidy HTML not found. If you'd like your HTML files to be pretty, install it from your distro's repository or from https://html-tidy.org"
fi
echo "✔ Tidy process succeeded."

rm -rf /var/www/vaydom/public/* && cp -r public/* /var/www/vaydom/public/
if [[ $? != 0 ]]; then
    echo "✘ Copying files to /var/www/vaydom/public/ failed."
fi

rm config.yaml
cd $MYDIR
