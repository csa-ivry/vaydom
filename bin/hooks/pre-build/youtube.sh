#!/bin/bash
# youtube.sh [run] [FILE1] [FILE2] ...
#   Takes an optional diff via stdin
#
# When called without arguments, returns a regex of interest
#
# Check for youtube shortcode in passed files. Download the associated preview
# image as static/external/youtube/ID.jpg. If a diff is passed via stdin,
# will check for youtube shortcode deletions. If a deletion occurs, will ensure
# that the video is not used elsewhere, and delete the associated image.

BASEREGEX="{{< +youtube +"
# Our main regex of interest, to detect youtube shortcodes
REGEX="$BASEREGEX\"?[a-zA-Z0-9_-]{11}\"?"

destinationDir=$(./bin/tools/mk-folder.sh "youtube")
[[ $? != 0 ]] && echo "[youtube.sh] ERROR: Cannot write to $destinationDir. Permissions problem?" && return 1

function findVideo {
    echo "$1" | grep -Po "[a-zA-Z0-9_-]{11}" && return 0
    return 1
}

function getVideo {
    [[ $# != 1 ]] && echo "[youtube.sh] ERROR: getVideo called with 0 or more than 1 videos. This should NEVER happen!" && exit 1
    
    youtubeId="$1"
    destinationFile="$destinationDir/$youtubeId.jpg"
    
    if [ -f "$destinationFile" ]; then
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[youtube.sh] DEBUG: Thumbnail for Youtube ID $youtubeID is already here."
        return 0
    fi
    
    curl --fail --silent -o "$destinationFile" "https://i.ytimg.com/vi/$youtubeId/maxresdefault.jpg"
    [[ $? != 0 ]] && echo "[youtube.sh] ERROR: Could not download preview image for video $youtubeId. Are you sure the id is correct?" && return 1

    ./bin/tools/img-reduce.sh "$destinationFile" "$destinationDir/$youtubeId.min.jpg"
    [[ $? != 0 ]] && echo "[youtube.sh] ERROR: Reducing image with bin/tools/img-reduce.sh failed." && return 1

    echo "[youtube.sh] Successfully imported preview image for video $youtubeId in $destinationDir."
    return 0
}

function runFiles {
    [[ $# = 0 ]] && return 1 # Should not be called without files

    for FILE in "$@"; do
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[youtube.sh] DEBUG: Examining $FILE"
        if [ ! -f "$FILE" ]; then 
            echo "[youtube.sh] ERROR: Passed file $FILE does not exist. This should NEVER occur (bug in pre-build.sh)!"
            return 1
        fi
        
        results=$(grep -Ps "$REGEX" "$FILE")
        [[ $? != 0 ]] && echo "[youtube.sh] ERROR: Passed file $FILE does not contain youtube shortcode. This should NEVER occur (bug in pre-build.sh)!" && return 1
        
        # Use read to break lines without breaking spaces
        echo "$results" | while read video; do
            # Clever trick for the error message!
            youtubeId=$(findVideo "$video")
            [[ $? != 0 ]] && echo "[youtube.sh] ERROR: Youtube video ID not found in $video. This should not happen!" && return 1
                
            [[ $PREBUILD_DEBUG = 1 ]] && echo "[youtube.sh] DEBUG: Found youtube ID $youtubeId in $1"        
            msg=$(getVideo "$youtubeId")
            [[ $? != 0 ]] && echo "$msg" && return 1 # Let getVideo handle the error messages
        done
    done
    return 0
}

function runChanges {
    # READ FROM STDIN FOR CHANGES
    # This allows us to collect the garbage in case of video deletion
    COUNTER=0
    
    read -t 0 x
    if [[ $? != 0 ]]; then
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[youtube.sh] DEBUG: No changes passed through stdin. No garbage to collect!"
        return
    fi

    cat /dev/stdin | while read x ;do
        DELETED=$(echo "$x" | grep -P "\-.*$REGEX")
        if [[ $? = 0 ]]; then
            video=$(findVideo "$DELETED")
            [[ $PREBUILD_DEBUG = 1 ]] && echo "[youtube.sh] DEBUG: Video $video was deleted. Checking if it's still used elsewhere…"
            grep -rPqs "$BASEREGEX\"?$video\"?" "$PREBUILD_CONTENT"
            if [[ $? != 0 ]]; then
                echo "[youtube.sh] The video is not used elsewhere! Deleting thumbnails…"
                # Using rm -f here because it does not produce noise. But if a permission
                # problem arises, it shows a clear and nice error message.
                rm -f "$destinationDir"/$video{,.min}.jpg && COUNTER += 1
            fi
        fi
    done

    [[ $COUNTER != 0 ]] && echo "[youtube.sh] Collected garbage for $COUNTER deleted video(s)."
    return 0
}

function garbageCollect {
    echo "[youtube.sh] Running garbage collector now…"
    for FILE in $destinationDir/*.min.jpg; do
        youtubeId=$(basename $FILE | cut -d"." -f1)
        grep -rPqs "$BASEREGEX\"?$youtubeId\"?" "$PREBUILD_CONTENT"
        if [[ $? != 0 ]]; then
            echo "[youtube.sh] Video $youtubeId has been deleted and is not used any more. Deleting the preview."
            rm "$destinationDir"/"$youtubeId"{,.min}.jpg
        fi
    done
    echo "[youtube.sh] Done collecting the garbage!"
    return 0
}

# If no argument is passed, we return the regex that interests us
# as that is what pre-build.sh expects us to do
if [[ $# = 0 ]]; then
    echo "$REGEX"
    exit
fi

case "$1" in
    "run")
        if [[ $# > 1 ]]; then
            runFiles "${@:2}"
            [[ $? != 0 ]] && echo "Building from files failed" && exit 1
        fi
        runChanges
        [[ $? != 0 ]] && echo "Running changes failed" && exit 1
        ;;
    "gc")
        garbageCollect
        ;;
    *)
        echo "[youtube.sh] ERROR: Unknown command $1"
        exit 1
esac
