#!/bin/bash
# ext-img.sh [run|gc] [FILE1] [FILE2] ...
#   Takes an optional diff via stdin
#
# When called without arguments, returns a regex of interest
# 
# run [FILE1] [FILE2] ...
#   Check for external image (Markdown syntax) in passed files
#   Download missing images as static/ext-img/HASH, and minify the image
#   as HASH.min.EXT. If a diff is passed via STDIN, will check in there for deletions.
# gc
#   Run garbage collector, i.e. find all leftover files that are no longer used, and delete them.

BASEREGEX="\!\[.*\]"
REGEX="$BASEREGEX\(.*\)"

destinationDir=$(./bin/tools/mk-folder.sh "ext-img")
[[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Cannot write to $destinationDir. Permissions problem?" && return 1              

function isRemote {
    [[ "$1" == http* ]] && return 0
    return 1
}

function isImported {
    [[ "$1" == "/"*"/ext-img/"* ]] && return 0
    return 1
}

function findImage {
    # findImage should catch all Markdown images passed to it (whether remote or not)
    # Catch closing ']' to avoid parenthesis within image description
    url=$(echo "$1" | grep -Po "\]\(.*\)") && echo "${url:2:-1}" && return 0
    return 1
}

# getImage URL [FILENAME]
# Extension is appended after FILENAME
function getImage {
    if [[ $# = 0 ]] || [[ $# > 2 ]]; then
        echo "[ext-img.sh] ERROR: getImage called with the wrong number of arguments ($#) : $@"
        exit 1
    fi
    
    img="$1"
    
    if [[ $# = 2 ]]; then
        destinationFile="$2"
    else
        destinationFile=$(echo "$img" | sha256sum)
        destinationFile=$"$destinationDir/{destinationFile:0:-3}"
    fi

    # Once we import the image, the path to it is changed so it should not match again
    # TODO: Upon successful download, check the image is not used elsewhere in content
        
    mimeType=$(curl -s -o /dev/null -w '%{content_type}' "$img")

    check=$(echo "$mimeType" | grep "^image/")
    [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Unknown mimetype for image $img. Are you sure this is an image?" && exit 1
    
    curl --fail --silent -o "$destinationFile" "$img"
    [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Could not download image $img" && exit 1
    
    # Only minimize jpeg
    mimeType=${mimeType:6}
    case "$mimeType" in
        "jpeg")
            
            ;;
        *)
            #echo "[ext-img.sh] Remote image is not JPEG. Not minimizing…"
            mv "$destinationFile" "$destinationFile.$mimeType"
            echo "$destinationFile.$mimeType"
            return 0
            ;;
    esac

    reducedFile="$destinationFile.min.jpg"

    ./bin/tools/img-reduce.sh "$destinationFile" "$reducedFile"
    [[ $? -ne 0 ]] && echo "[ext-img.sh] ERROR: Reducing image $destinationFile failed." && exit 1
    
    echo "$reducedFile"
    
    return 0
}

function runFiles {
    [[ $# = 0 ]] && echo "[ext-img.sh] ERROR: runFiles should never be called without files. Bug?" && return 1

    for FILE in "$@"; do
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[ext-img.sh] DEBUG: Examining $FILE"
        [ ! -f "$FILE" ] && echo "[ext-img.sh] ERROR: Passed file $FILE does not exist. This should NEVER occur (bug in pre-build.sh)!" && return 1
        
        results=$(grep -Ps "$REGEX" "$FILE")
        [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Passed file $FILE does not contain any external images. This should NEVER occur (bug in pre-build.sh)!" && return 1
        
        # Use read to break lines without breaking spaces
        echo "$results" | while read input; do
            imgURL=$(findImage "$input")
            [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: External image not found in $results. This should not happen!" && return 1
            
            # Filter for only remote content (i.e. local images are not our interest)
            isRemote "$imgURL" || continue
            
            destinationFile=$(echo "$imgURL" | sha256sum)
            destinationFile="$destinationDir/${destinationFile:0:-3}"

            [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: External image not found in $input. This should not happen!" && return 1

            # TODO: capture error messages using STDERR
            targetFile=$(getImage "$imgURL" "$destinationFile")
            [[ $? != 0 ]] && echo "$targetFile" && return 1
            [[ $PREBUILD_DEBUG = 1 ]] && echo "[ext-img.sh] DEBUG: Image $imgURL successfully downloaded"
            
            # Now we need to edit the file in question.. Strip "static/" from string
            targetFile=${targetFile:7}
            [[ $PREBUILD_DEBUG = 1 ]] && echo "[ext-img.sh] DEBUG: Replacing $imgURL with /$targetFile in $FILE"
            # sed does regex matching, which we really don't want in this specific case
            replace -s "$imgURL" "/$targetFile" -- "$FILE"
            [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Could not edit file $FILE to replace link to image $targetFile." && return 1
            
            echo "[ext-img.sh] Successfully imported image from $imgURL."
        done
    done
    
    return 0
}

function runChanges {
    # READ FROM STDIN FOR CHANGES (diff)
    # Allows to collect garbage for our own deleted content
    COUNTER=0
    read -t 0 x
    [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: runChanges should not be called with empty STDIN!" && return 1

    cat /dev/stdin | while read x ;do
        DELETED=$(echo "$x" | grep -P "\-.*$BASEREGEX")
        # Skip when we don't match removal
        [[ $? != 0 ]] && continue
        
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[ext-img.sh] DEBUG: Content $DELETED was removed."

        image=$(findImage "$DELETED")
        [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: External image not found in STDIN. This should not happen!" && return 1
        
        # Filter for only imported content
        isImported "$image" || continue
        
        [[ $PREBUILD_DEBUG = 1 ]] && echo "[ext-img.sh] DEBUG: Image $image was deleted. Checking if it's still used elsewhere…"
        grep -rPqs "$BASEREGEX\($image\)" "$PREBUILD_CONTENT"
        # Not deleting image because it's still used
        [[ $? = 0 ]] && continue

        echo "[ext-img.sh] The image $image is not used elsewhere! Deleting it…"
        # Using rm -f here because it does not produce noise. But if a permission
        # problem arises, it shows a clear and nice error message.
        rm -f "$destinationDir"/"$image" && COUNTER += 1
    done

    [[ $COUNTER != 0 ]] && echo "[ext-img.sh] Collected garbage for $COUNTER deleted image(s)."
    
    return 0
}

function garbageCollect {
    echo "[ext-img.sh] Running garbage collector now… NOT IMPLEMENTED YET"
    # TODO: Implement garbage collect. Or can this be left to a generic unused resources collector?
}

# If no argument is passed, we return the regex that interests us
# as that is what pre-build.sh expects us to do
if [[ $# = 0 ]]; then
    echo "$REGEX"
    exit
fi

case "$1" in
    "run")
        if [[ $# > 1 ]]; then
            runFiles "${@:2}"
            [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Running from matching files failed." && exit 1
        fi
        runChanges
        [[ $? != 0 ]] && echo "[ext-img.sh] ERROR: Running from matching changes failed." && exit 1
        ;;
    "gc")
        garbageCollect
        ;;
    *)
        echo "[ext-img.sh] ERROR: Unknown command $1" && exit 1
esac

exit 0
