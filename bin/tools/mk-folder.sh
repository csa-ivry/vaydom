#!/bin/bash

function help {
    echo "mk-folder.sh PLUGIN_NAME"
    echo "--------"
    echo "Will create a folder for PLUGIN_NAME in static/external"
    echo "Will return the path to the folder upon success, exit 1 otherwise"
}

if [[ $# = 0 ]]; then
    help
    exit 0
fi

target="$PREBUILD_STATIC"/external/"$1"
echo "$target"
mkdir -p "$target" || exit 1

