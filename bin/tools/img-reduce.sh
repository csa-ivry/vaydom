#!/bin/bash

if [[ $# != 2 ]]; then
    echo "[img-reduce.sh] Wrong number of arguments. Need input and output file, received $@"
    exit 1
fi

if [ ! -f "$1" ];  then
    echo "[img-reduce.sh] Requested input file does not exist : $1"
    exit 1
fi

mogrify -write "$2" -filter Triangle -define filter:support=2 -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 60 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip "$1"

[[ $? = 0 ]] || echo "[img-reduce.sh] Could not reduce, or save output $2. Maybe a permissions problem?"
