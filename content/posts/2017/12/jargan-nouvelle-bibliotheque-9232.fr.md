---
title: "Jargan, nouvelle bibliothèque autogérée"
date: 2017-12-12T22:11:42Z
draft: false
image: "2017/12/786ddde14d1e413ec474d947b7a2c5.jpg"
description: "À toutes les amoureuses et amoureux de la lecture, nous avons le plaisir de vous annoncer la création d’une bibliothèque autogérée au CSA Vaydom à Ivry-sur-Seine : Jargan !"
---

À toutes les amoureuses et amoureux de la lecture, nous avons le plaisir de vous annoncer la création d’une bibliothèque autogérée au CSA Vaydom à Ivry-sur-Seine : Jargan !

Le Centre Social Autogéré Vaydom permet à des personnes et à des familles précarisées d’avoir un toit et de vivre décemment. C’est aussi un espace politique et anti-autoritaire de rencontres, d’activités et d’événements : projections de films, débats, cours de sport et de langues, infokiosque, ateliers théâtre, cantine populaire…

À l’image du CSA, Jargan est ouverte à tou·tes. Tou·tes les membres sont sur un pied d’égalité, et vous êtes tou·tes les bienvenu·es pour faire vivre ce lieu, participer aux événements, lire sur place ou emprunter des ouvrages gratuitement, dans le cadre des horaires d’ouverture au public du Vaydom.
Si vous avez des livres à donner, ou si vous souhaitez apporter votre aide de manière ponctuelle ou régulière (par exemple pour l’accueil des lecteurs et lectrices, l’organisation d’événements, le classement des livres ou la fabrication d’étagères), n’hésitez pas à contacter Jargan ou à lui rendre visite directement.

La curiosité de Jargan est infinie. Elle s’intéresse à tous les types d’ouvrages (tant qu’ils sont compatibles avec les valeurs du Vaydom), dans toutes les langues : bandes dessinées, romans, essais politiques et sociologiques, revues militantes, livres d’histoire, d’art, d’anthropologie, de philosophie, de sciences…

Parce qu’elle pense que la lecture est une invitation à sortir de soi pour aller vers l’autre, Jargan souhaite devenir un lieu de rencontres et de partage, et recherche tout particulièrement des livres qui éveillent les consciences, qui développent l’esprit de résistance à la domination et qui donnent les outils pour construire un monde plus juste, égalitaire, solidaire, antiraciste, féministe, écologique et respectueux des droits de toutes les minorités.

Une permanence aura lieu chaque dimanche entre 16 h et 20 h, avec un espace informel de discussions improvisées et un espace dédié aux événements littéraires et politiques organisés par Jargan : présentation d’ouvrages ou de revues, lectures publiques suivies de débats, ateliers contes pour les enfants… Jargan est ouverte à toutes vos suggestions pour étoffer son programme. On pourra ensuite prolonger la soirée avec un délicieux repas à la cantine populaire (autogérée et à prix libre).

Le lancement de Jargan est prévu le dimanche 17 décembre entre 16 h et 20 h, avec la présentation de <a href="http://pantherepremiere.org/">Panthère Première</a>. C’est une revue de critique sociale animée par un collectif non-mixte, dans un esprit féministe. Nous découvrirons dimanche son premier numéro, autour du dossier « Quiproclash ! Mordre et se faire mordre la langue » Au programme, la lecture de textes de cette revue ainsi que d’autres que nous pourrons choisir ensemble.

Vous pouvez contacter Jargan à l’adresse suivante : jargan@riseup.net.

Si vous souhaitez en savoir plus sur le Vaydom, nous vous invitons à lire les articles du site Paris-luttes-info : « <a href="https://paris-luttes.info/il-semblerait-qu-on-aie-un-nouvel-8642">Le Vaydom, nouveau centre social autogéré à Ivry</a> », « <a href="https://paris-luttes.info/portes-ouvertes-et-cantine-8432">Portes ouvertes et cantine populaire au Centre social autogéré d’Ivry</a> ».

Les livres pour enfants et les manuels scolaires sont aussi les bienvenus, merci de les déposer au rez-de-chaussée à l’espace des enfants.
