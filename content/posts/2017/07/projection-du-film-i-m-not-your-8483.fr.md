---
title: "Projection du film « I’m not your negro » le 13 juillet au CSA d’Ivry"
date: 2017-07-12T18:35:02Z
draft: false
image: "2017/07/b5ca9e8f9e3c286954931d13be28ef.png"
description: "Récit radical sur la question raciale aux États-Unis. Projection/débat autour du film, le 13 juillet au CSA d'Ivry."
---

![](/images/2017/07/arton8483-47d8a.png)
###  « I’m not your negro »
#### Les luttes anti-racistes aux USA

Basé sur un texte inédit de l’écrivain afro-américain James Baldwin (1924-1987), ce film est un récit radical sur la question raciale aux États-Unis, en utilisant exclusivement les mots-mêmes de l’auteur. Il revient notamment sur les assassinats de figures comme Martin Luther King Jr., Medgar Evers et Malcolm X.

Raoul Peck, réalisateur du film :

« Ce film parle du présent et porte un regard engagé sur ce qui s’y passe. (…) Je pense qu’il arrive à point nommé, quand on voit la recrudescence des violences à l’encontre des Noirs et autres minorités (incluant également aujourd’hui les musulmans). Les mots de Baldwin, malheureusement, parlent de ce que nous vivons aujourd’hui, non seulement outre-Atlantique mais aussi en Europe, comme en témoigne le regard dirigé vers les étrangers sur ce continent, pourtant nourri d’immigration. Ce va-et-vient entre les images d’actualité d’il y a cinquante ans et celles d’aujourd’hui permet de juxtaposer les policiers d’autrefois, avec des matraques et des chiens, et ceux d’aujourd’hui, dotés d’un armement militaire. Cette logique de répression que le film met bout à bout raconte l’histoire de cette violence d’État. » ([source](http://www.frenchtouch2.fr/2017/05/im-not-your-negro-entretien-avec-raoul.html))

Nous vous accueillons à partir de 19h30, pour débuter la projection à 20h. Il y aura de quoi grignoter après le film.

Trailer :

{{< youtube "rNUYdgIyaPM" >}}
