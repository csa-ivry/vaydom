---
title: "Soirée d’information et de discussion sur la Tchétchénie"
date: 2017-08-23T21:21:34Z
draft: false
image: "2017/08/5d7b7b182438d79cbba76e01b913df.jpg"
description: "La guerre de Tchétchénie est officiellement finie depuis plus de 10 ans. Mardi 5 septembre 2017 à 19 heures, soirée d’information et de discussion sur la Tchétchénie."
---

![](/images/2017/08/arton8578-92a8f.jpg)

La guerre de Tchétchénie est officiellement finie depuis plus de 10 ans. Pourtant, l’hémorragie se poursuit et le peuple tchétchène continue de se déverser sur les routes de l’Europe. Les rares qui ont voulu faire la lumière sur les exactions et les atrocités commises dans le Caucase du Nord ont été assassinéEs et seuls les défenseur-euses des droits humains semblent encore pleurer leur mémoire.

Mais que se passe-t-il donc en Tchétchénie ?

Plusieurs familles tchétchènes habitent le Centre social autogéré d’Ivry. C’est l’occasion de faire une rétrospective des événements qui se déroulent dans leur pays d’origine, afin de contribuer à ce que le silence ne règne pas totalement sur le destin de leur(s) peuple(s).

Mardi 5 septembre 2017 à 19 heures, soirée d’information et de discussion sur la Tchétchénie au Centre Social Autogéré d’Ivry sur Seine (37 rue Marceau, métro 7 « Pierre et Marie Curie »)
