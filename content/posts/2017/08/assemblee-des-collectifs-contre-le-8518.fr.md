---
title: " Assemblée des collectifs contre le Grand Paris, GPII et démolition des quartiers "
date: 2017-08-30T11:32:35Z
draft: false
image: "2017/08/73bfe683215a7926138eb7a70e0262.jpg"
description: "Après midi de rencontres, discussions et mise en place d’initiatives contre le Grand Paris, le samedi 2 septembre à partir de 14h."
---

Après midi de rencontres, discussions et mise en place d’initiatives : samedi 2 septembre à partir de 14h au Centre Social Autogéré, 37 rue Marceau, Ivry sur Seine.

![](/images/2017/08/arton8518-bb91f.jpg)

Appel aux nombreux collectifs qui luttent contre des Grands Projets Inutiles Imposés et démolitions du Grand Paris

Au sein de la COSTIF (Coordination pour la Solidarité des Territoires d’Île-de-France et contre le Grand Paris), du DAL (Droit au Logement), et d’autres mouvements, nous sommes nombreux à lutter sous différentes formes contre le Grand Paris et son projet de mégapole, avec ses conséquences comme la démolition de quartiers populaires et de logements sociaux, la marchandisation du patrimoine public, la spéculation immobilière et foncière, la crise du logement, les politiques d’urbanisme autoritaires entretenus et accélérés par les Jeux Olympiques, l’exposition universelle...

![](/images/2017/08/arton8518-c5e5a-14bf3.jpg)

Tout cela se fait au profit des grands groupes, des promoteurs, des milieux du BTP et entraîne la hausse des prix des loyers et le logement cher, la disparition des terres agricoles, la dégradation de notre environnement, les expulsions et l’épuration sociale des quartiers populaires...

Ne nous laissons pas faire, organisons-nous ! Nous pouvons être plus forts, contre ces intérêts puissants. Pour mieux nous coordonner et élaborer des projets d’actions très larges, rencontrons-nous.

Après midi de rencontres, discussions et mise en place d’initiatives : samedi 2 septembre à partir de 14h au Centre Social Autogéré, 37 rue Marceau, Ivry sur Seine.

Liste des collectifs ayant déjà répondu présents à cet appel :

AGIV (Gentilly), APPUII, Cité Jardin de la Butte Rouge (Châtenay-Malabry), ARIVEM (association des riverains de l’usine de méthanisation de Romainville), Collectif 3R (Ivry-sur-Seine / Paris XIII), Collectif Baron Leroy (Paris : Bercy Charenton), collectifs actifs pour la préservation du Parc de La Courneuve (Collectif pour la défense du Parc de La Courneuve - Georges Valbon et collectif Notre parc n’est pas à vendre), CSA (Ivry-sur-Seine), COSTIF, CPTG (Triangle de Gonesse), DAL, DAL HLM, Environnement 92, Journal charIVaRY, La Révolution est en marche (Aulnay-sous-Bois), Métr’Auber (Aubervilliers), Mur à pêche (Montreuil) Non aux JO 2024 à Paris, Association des locataires du quartier de La Coudraie (Poissy), Quartier Ledaux Le Plessis Robinson, Terres fertiles (Saclay), ZAD Patate (Montesson)
