---
title: "Le « Vaydom » nouveau centre social autogéré à Ivry"
date: 2017-09-08T08:50:47Z
draft: false
image: "2017/09/415f6899e8dd426dd1bca4bbcac123.jpg"
description: "Un Centre Social Autogéré à Ivry-sur-Seine ? Présentation et petit historique..."
---

![](/images/2017/09/arton8642-d4150.jpg)
### Notre entrée dans les lieux

En février 2015, on apprenait la mise sous sauvegarde de justice de La Mutuelle des Etudiants (LMDE), qui croulait sous les dettes. Le 25 juin suivant, ses  salariés menaient une grève devant le siège social situé 37 rue Marceau à Ivry-sur-Seine, tentant de sauvegarder leurs emplois avant la reprise de leur boîte par une autre mutuelle ou par la Caisse nationale d’assurance maladie.

A l’été, le bâtiment était vidé et certaines fenêtres fermées par des plaques de métal soudées dans les parois. A l’intérieur, la lumière reste allumée durant des mois et des fenêtres battent au vent.

Laissé à l’abandon pendant deux années, notre petite équipe décide de lui redonner vie. En avril 2017, à la recherche d’un lieu pour héberger des activités politiques, et où vivre décemment malgré notre précarité, nous commençons à nous intéresser à ce bâtiment. Étant donné la facilité que nous avons à entrer dans les lieux, nous décidons d’une date d’aménagement.

Le 27 mai, nous occupons l’immeuble. L’emménagement se fait entre 9 heures et 12 heures, sans que personne ne nous dérange. Vers 12h30 se tient la première assemblée du lieu.

Le chef de la sécurité débarque après la bataille et prétend que nous avons « maîtrisé » un agent de sécurité. Sans savoir que la porte était restée ouverte et que l’armoire à clés du rez de chaussé nous donnait l’accès à tout le reste du bâtiment…

La police arrive dans la foulée, mais se contente de prendre acte de notre présence, relève quelques identités et procède à une rapide enquête de voisinage. Le sort en est jeté.

### Qui sont nos détracteurs ?

Pour nous le squat est une réponse collective à la précarisation de nos vies, une manière de s’attaquer à la propriété immobilière, qui envoie de plus en plus de personnes à la rue, et un moyen de lutter collectivement contre la gentrification de nos quartiers.

Le cadastre indique que l’immeuble est géré par la copropriété du 37 bis. Dans les faits, et l’assignation en justice nous l’apprendra, le propriétaire est la banque Crédit Mutuel, qui fait gérer le bien par la société immobilière Nexity, qui la sous-traite elle-même à la société de gestion d’actifs La Française.

Et tout en bas de l’échelle, la surveillance revient à l’agence de sécurité privée Kheops Sécurité.

Un bien beau montage !

L’huissier de justice se présente à nous bientôt, sans policiers. On choisit de le laisser entrer. Il produit un constat sans avoir pris le temps d’y noter touTEs les habitantEs, mais en précisant la présence de cuisines et de sanitaires, puis nous délivre une assignation heure à heure pour une audience devant le tribunal d’instance d’Ivry-sur-Seine le 7 juillet 2017.

### Où en est la procédure à ce jour ?

On obtient un report au 12 septembre sur la base des demandes d’aide juridictionnelle. La juge refuse l’ensemble des intervenantEs volontaires, c’est-à-dire touTEs les habitantEs qui n’étaient pas inscritEs sur le constat d’huissier.

L’huissier s’est présenté à nouveau en juillet pour réaliser un nouveau constat, sur lequel il ajoute la quinzaine d’intervenantEs volontaires pour lesquelLEs des demandes d’aide juridictionnelle avaient été déposées avant l’audience du 7 juillet.

Désormais, la juge devra tenir compte du fait qu’au moins 30 personnes habitent le bâtiment.

### Qu’est-ce qu’on fait là ?

Dans les faits, ce sont une cinquantaine d’habitantEs qui vivent là, dont une vingtaine d’enfants.
A cela s’ajoute un collectif, ouvert à touTES celleux qui le souhaitent, qui compte une vingtaine de personnes non-habitantes qui font vivre les espaces collectifs du bâtiment.

Avant même de choisir ce bâtiment, nous avions des envies. Nous voulions notamment ouvrir un gros lieu qui permette d’organiser des événements politiques, susciter des rencontres et des débats, être identifié comme un lieu de résistance au carnage capitaliste environnant. CertainEs ont évoqué l’absence de gros lieu politique depuis la fermeture du Transfo à Bagnolet fin octobre 2014. Il était donc clairement question de rouvrir des possibles révolutionnaires sur Ivry (si si).

Puis, au regard de sa taille et connaissant déjà des centres sociaux « à l’italienne » et les expériences de la CREA à Toulouse et de l’Attieké à Saint-Denis en France, on s’est dit surtout que l’espace permettrait à des personnes et familles sans toit d’en avoir un, sans abandonner nos désirs et nos besoins d’espaces politiques. C’est ainsi qu’on s’est associéE à des personnes et familles qui voulaient bien « la jouer collective ».

### Comment on s’organise ?

Le bâtiment est donc organisé comme suit : un nombre limité d’habitantEs (une cinquantaine) occupent et vivent dans les deuxième et troisième étages du bâtiment, tandis que le rez-de-chaussée, les premier et quatrième étages sont collectifs et ouverts à touTEs. Chaque étage d’habitation est divisé en deux, chaque demi étage ayant ses sanitaires, sa cuisine et son salon commun. Les habitantEs organisent elleux-mêmes la vie de leur étage, à condition que cette organisation ne remette pas en cause l’organisation générale du lieu.

Chaque dimanche se tient une assemblée des habitantEs, tandis que l’organisation des espaces collectifs se met en place lors de réunions ouvertes (donc publiques) qui se tiennent tous les lundi soir à 19h. Les lundis soir, on espère voir affluer les propositions d’actions politiques et d’activités au sein du lieu !

Tous les jeudis à 20h ont lieu des projections dans notre salle de cinéma. Une permanence d’accès au droit des étrangerEs est déjà ouverte ainsi que des cours de français et de russe. N’hésitez pas à venir étoffer ce programme !

Chaque dimanche soir se tient une cantine populaire ouverte à touTEs ! C’est le moment choisi pour une première rencontre du lieu et de ses occupantEs.

Enfin et surtout, ce lieu est anti-autoritaire. Si des comportements dominants apparaissent, nous ferons notre possible collectivement pour les faire échouer. Tous les comportements et/ou propos sexistes, racistes, homophobes, transphobes, validistes ou relevant de n’importe quelles autres formes de domination n’ont pas leur place ici. On ne veut pas non plus d’élitisme, même s’il se dit anarchiste ou autonome.

Dans ce lieu, on vit collectivement et on s’organise collectivement : c’est la tache de touTEs de le nettoyer et de l’aménager, pour qu’il soit vivable pour touTEs.

Ce lieu se veut ouvert à touTEs : il suffit de l’investir, vous êtes les bienvenuEs !

Longue vie au centre social autogéré d’Ivry !

Le collectif du "Vaydom" (Notre Maison en Tchétchène)

![](/images/2017/09/csa_presentation-a9a97-d4a72-2fbaf.jpg)
