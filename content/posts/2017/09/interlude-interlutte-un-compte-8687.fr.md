---
title: "Interlude Interlutte : un compte-rendu de la discussion"
date: 2017-09-20T17:59:37Z
draft: false
image: "2017/09/026be40c40fa9fb9a10a0a52fc03fb.jpg"
description: "Suite à « Interlude Interlutte », un compte rendu subjectif et le moins partiel possible de la discussion."
---

Le 13 septembre à 18h, une soirée de convergence des luttes « Interlude Interlutte » était organisée pour discuter ouvertement de la manif du 12 septembre et de la suite du mouvement. Voici un compte rendu subjectif et le moins partiel possible de la discussion.

Je suis arrivé au CSA d’Ivry sur les coups de 19h, sachant qu’un mercredi soir, lendemain de manif, les retardataires seraient nombreux/ses. Cela se passe au Centre Social Autogéré (CSA) d’Ivry, récemment baptisé « Vaydom ». Lorsque les organisateurs du Paul Technique lancent la discussion, nous ne sommes pas plus d’une vingtaine.

C’est un des résidents du lieu qui nous accueille en présentant rapidement le CSA, son histoire et son fonctionnement - pour en savoir plus, vous pouvez lire leur article de présentation : [Le « Vaydom » nouveau centre social autogéré à Ivry]({{< relref "posts/2017/09/il-semblerait-qu-on-aie-un-nouvel-8642.fr.md" >}})

Il en profite pour nous donner des nouvelles de l’audition qui s’est tenue la veille, plutôt positive ; une nouvelle date dans la procédure qui les oppose au bailleur est fixé en octobre. Leur assemblée est fixée tous les lundi à 19h ; le dimanche c’est portes ouvertes et dîner collectif.

La discussion démarre par un tour de parole sur la manifestation de la veille et le ressenti de chacun.e. concernant le cortège de tête. La parole passant tout d’abord de voisin.e à voisin.e, les voix d’hommes et de femmes se relaient pas mal. Je dirai que dans l’ensemble, la parité a été presque respectée ; du moins, il y a eu un effort tangible de toute part pour que celle-ci s’impose.

### Retour sur le 12 septembre

Tout le monde constate qu’au-delà des chiffres annoncés ici et là, le cortège de tête attire du monde et se massifie, ce qui est un fait positif dans la mesure où il semble avoir acquit une véritable légitimité au sein du mouvement social. Cependant l’arrivée de (trop ?) nombreuses personnes peu habituées au cortège, ainsi qu’au niveau de répression qu’il a l’habitude de subir pose un certain nombre de problèmes : mouvements de foule et divers comportements à risque. Il a été exprimé le besoin d’informer de façon très pratique sur ce qu’est le cortège de tête, les risques associés à sa participation et les bons réflexes à avoir en terme d’équipement et de comportement. Déjà largement disponible sur les sites web militants et les lieux tels que le CSA, une telle documentation demande à circuler durant les manifs au sein du cortège de tête. D’autre part, il a été remarqué la rareté des slogans (toujours les mêmes).

Après avoir évoqué le fait que les grands médias ne relayaient que très peu d’informations sur les manifs et encore moins, voire pas du tout, sur le cortège de tête, plusieurs personnes évoquent la perception extérieure de cette forme d’action. On en vient à parler du black block qui, sans être remis en cause, est interrogé sur sa capacité à mobiliser au-delà d’un cercle militant de plus en plus enclin à porter le noir. Le cas des lycéens semble exemplaire : le passage entre un état de faible politisation (le cadre du lycée) à la participation au black block peut paraitre trop brutal à beaucoup et, à terme, démobiliser. Certain.e.s évoquent la peur qui frappe les lycéen.ne.s au vue de la violence des manifestations lycéennes de 2016 et de l’entre-deux tours. La difficulté à mobiliser les lycéens pose problème car il s’agit d’une véritable force, bien plus explosive et potentiellement révolutionnaire que les simples militants CGT/FI.

La question du dispositif policier a également été discutée par plusieurs participant.e.s. Son caractère allégé s’explique-t-il par les soucis internes à la corporation ? S’agissait-il pour l’État de faire de la manifestation parisienne une « vitrine » d’une police apaisée, tandis qu’à Lyon ou Nantes la répression s’abattait implacablement sur les cortèges ? L’absence de moments de conflictualité durant la plus grande partie du trajet a-t-elle vocation à neutraliser le cortège de tête et sa dimension déter ? Car de nombreux/ses participant.e.s ont relevé un cortège bien moins offensif/déter que lors des dernières manifestations. Est-ce du seulement à l’allégement du dispositif policier ? Une participante a tenu à rappeler que, malgré son caractère « light », le dispositif policier demeurait réellement imposant et manifestement liberticide. Une autre personne a rebondi là dessus pour dire qu’en soit cette façon d’encadrer les manifs constituait une véritable violence.

### Solidarité entre les cortèges 

Un autre point discuté a concerné la relation avec les autres cortèges/organisations présent.e.s dans la manifestation du 12 septembre. On a évidemment parlé des forains et de leur roi aux amitiés fascisantes et aux pratiques mafieuses. Si ces derniers ont permis de constituer un tampon entre le cortège de tête et les gros bras du SO de la CGT - qui ne se sont pas privé de [violemment molester cinq camarades féministes du cortège de tête](https://paris-luttes.info/agression-violente-de-5-femmes-a-8683).


Il semble acquis que cette présence, parfois amusante et/ou pratique, constitue un véritable problème qui, si Macron ne cède pas à leur revendication, mérite discussions et prises de position.

La question des syndicats, de leurs cortèges et de la direction du mouvement social a fait l’objet de nombreuses interventions. Cela a été l’occasion pour certain.e.s de développer une critique radicale du cortège de tête, et notamment de toute la littérature qui lui est associée ; celle-ci aurait eu tendance à l’encenser, à défaut de permettre l’émergence d’une critique constructive de ce qui s’y passe. Car bien qu’il ait acquis une certaine légitimité et qu’il attire, le cortège de tête demeure dépendant du calendrier fixé par les centrales syndicales. La critique exprimée par plusieurs personnes touche également à l’incapacité de déborder du trajet fixé par la préfecture et les syndicats, ce qui tranche avec le chant « Et la rue, elle est à qui ? Elle est à nous » !

C’est en partant de cette problématique que la petite assemblée, qui compte désormais plus d’une soixantaine de personnes, a été amenée à discuter de l’autonomie : sa mise en œuvre au sein du cortège de tête, dans la vie de chacun.e. et dans la perspective d’un mouvement social fort.

Un participant a témoigné de son hésitation à rejoindre ou pas le cortège de tête, exprimant cette défiance par non seulement la dimension « obligatoire » du cortège de tête pour tout militant aspirant à l’autonomie, mais surtout le problème que pose la rupture avec le reste de la manifestation, ce qui a été manifeste le 12 septembre. Au-delà de la question des forains et des SO syndicaux, la question se pose de fluidifier le mouvement entre les deux espaces. Il a également été avancé par plusieurs personnes le besoin de fixer une temporalité différente de celle fixée par les syndicats et partis politiques, c’est-à-dire être en mesure de poser des dates pour des actions, mais également des manifestations. Sans que le Front Social ne soit explicitement mentionné, il a été fait état de la volonté au sein même des syndicats de s’émanciper du calendrier fixé par les grandes centrales et construire un mouvement de blocage et de grève générale.

### Construire l’autonomie

Plusieurs retours d’expérience, ainsi que des analyses du mouvement de 2016 ont été proposées à l’assemblée. Les possibilités offertes par l’occupation de la place de la République durant le mois d’avril 2016 ont été évoquées pour souligner le besoin de disposer d’un point central où les différents groupes affinitaires pourraient se rencontrer, échanger et construire des actions ensemble. Une autre idée, tirée du déplacement des zadistes de NDDL à la marche du 19 mars, a été défendue : à l’instar de leur cantine, il faut investir le cortège de tête avec les différentes pratiques et expériences de l’autonomie ; rebondissant sur cette idée, une personne a fait remarquer qu’il était impossible de trouver de la nourriture vegan aux manifestations.

Une intervenante a attiré l’attention de l’assemblée sur la question des personnes ciblées par la politique gouvernementale et, plus largement, l’ensemble du système tenu par l’État, prenant comme exemple les bénéficiaires de l’APL qui ne souffrent pas seulement de la baisse des aides, mais de leur assujettissement à celles-ci. Elle avance qu’en négligeant ces différents cas, les mouvements sociaux et politiques perdent en force et en crédibilité. Il est donc nécessaire de répertorier l’ensemble des attaques ciblées et réfléchir à la réponse collective à apporter. Dans le même ordre d’idée, il a été avancé qu’à la vue du faible impact de la manifestation, il était nécessaire de s’investir dans une construction quotidienne de l’autonomie, aussi bien à l’échelle individuelle que collective, ce sur quoi un habitant du CSA a témoigné de l’expérience du collectif qui a ouvert le centre et a appelé à ouvrir un maximum de lieux. S’est ensuite engagée une discussion autour de la place accordée à telle ou telle échelle : doit-on privilégier l’action individuelle ou chercher à construire collectivement, au-delà de groupes affinitaires parfois bien établis.

Construire l’autonomie… Oui mais où ? Comment ? Avec qui ? Et surtout : dans quel but (une interrogation posée de façon très concrète par un participant) ? Les organisateurs ont proposé de discuter de tout cela la semaine prochaine, mercredi 20 septembre, non pas en assemblée mais par petits groupes, ce qui devrait permettre à tou.te.s de prendre la parole (une demande d’une participante qui semble avoir fait consensus).
Avant de passer au repas, quelques annonces ont été faites :

Concernant l’affaire de la voiture brûlée quai de Valmy, il a été appelé à :

- une discussion au CICP le jeudi 14 septembre.
- une soirée place des Fêtes avec des interventions, une discussion et des actions (de collage notamment) le lundi 18 septembre
- un rdv au TGI de Paris le mardi 19 septembre à partir de 13h30. Le syndicat alliance ayant appelé à faire présence, il a été demandé de venir soutenir les camarades inculpés.

Concernant la coordination anti-répression :

La coordination continue d’opérer et cherche des bonnes volontés, notamment pour tenir le standard téléphonique. Il n’est pas besoin d’un savoir-faire particulier. Le contact n’a pas changé : 07 53 13 43 05 et stoprepression@riseup.net

Il est 21h30, la partie festive de la soirée peut commencer.

