---
title: "Assemblée Féministe dimanche 10 septembre à 14h au CSA d’Ivry"
date: 2017-09-07T20:58:44Z
draft: false
image: "2017/09/d84975ad6ac334a22c12a499ba5eaf.png"
description: "Organisons-nous pour nous défendre, au cœur des luttes des plus stigmatisées, réprimées, et précaires d’entre nous ! RDV le 10 septembre à 14h au CSA d'Ivry."
---

### L’AG féministe ouverte inclusive et intersectionnelle organisatrice de la marche du 8 mars poursuit ses réunions au centre social autogéré d’ivry.

![](/images/2017/09/21462795_895570630584163_2858692125780598415_n-005bb-710fb.png)

Notre vision du féminisme se fonde sur la reconnaissance de la totale légitimité des premièrEs concernéEs à décider de leurs stratégies de lutte contre les violences et discriminations qu’elles subissent.

Organisons-nous pour nous défendre, au cœur des luttes des plus stigmatisées, réprimées, et précaires d’entre nous ! »

### Les prises de décisions de l’AG du 4 juin :
#### Fonctionnement, principes et pratiques de l’AG :

L’AG a décidé de formaliser son fonctionnement, ses principes et pratiques (charte) c’est-à-dire de les définir et de les rendre publique afin de rassembler et de construire un espace de lutte accessible à touTEs.

Parmi ses fonctionnements, principes et pratiques nous avons commencé par là et nous proposons lors de la prochaine AG de continuer :

    L’AG est ouvertES aux militantEs et non militantEs féministEs.
    L’AG est non mixte* femmes et minorité de genre.
    L’AG est libre de s’organiser en non mixités.
    L’AG soutient l’autodéfense collective.

Toutes les initiatives mixtes de genre excluent les mecs cis* agresseurs.

Rendez vous le dimanche 10 septembre à 14h au CSA (Centre Social Autogéré) d’Ivry, 37 rue Marceau Ivry-sur-Seine, Metro 7 – Pierre et Mairie Curie, Tram 3 – Maryse Bastié.
