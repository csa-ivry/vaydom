---
title: "Présentation-discussion sur les armements du maintien de l’ordre"
date: 2017-09-07T11:16:05Z
draft: false
image: "2017/09/650779bb82ff1b52dff90dabd9a0f9.jpg"
description: "Discussion-rencontre avec des street medics et des membres du collectif Désarmons-les. Le 7 septembre à 19h au CSA d'Ivry."
---

![](/images/2017/09/arton8579-c89b9.jpg)
L’intensité des conflits sociaux récents a exposé de nombreuses personnes à une violence à laquelle elles n’étaient pas habituéEs. Cette brutalité n’est pourtant ni nouvelle, ni désorganisée, le maintien de l’ordre étant un art dont l’État français est maître de longue date. Et la férocité de la répression est proportionnelle à la violence sociale produite par le capitalisme.

À l’approche de nouvelles mobilisations sociales d’ampleur, il peut être utile de connaître mieux les moyens de l’ennemi et d’échanger sur les moyens de se protéger et de se défendre.

Le jeudi 7 septembre 2017 à 19 heures, nous vous invitons donc à une soirée d’information et de discussion sur les armements du maintien de l’ordre animé par des membres du collectif Désarmons les et des Medics, au Vaydom, Centre Social Autogéré d’Ivry sur Seine (37 rue Marceau, métro 7 « Pierre et Marie Curie »).


#### [Suite au 15 août à Bure : autopsie de la grenade « assourdissante » GLI F4](https://paris-luttes.info/suite-au-15-aout-a-bure-autopsie-8576)
22 août - Au cours de la manifestation contre l’enfouissement des déchets nucléaires à Bure, des affrontements ont éclatés entre opposantEs au projet Cigéo et gendarmes sur les champs entourrant la commune de Saudron (Meuse). Peu (...)

#### [Suite au 1er mai : autopsie de la grenade de désencerclement](https://paris-luttes.info/suite-au-1er-mai-autopsie-de-la-5556)
31 mai 2016 - Lors de la manifestation du 1er mai à Paris, les forces de l’ordre ont eu recours de manière intensive aux grenades dites « de désencerclement », occasionnant un grand nombre de blessé-e-s, dont certain-e-s gravement. C’est (...)


![](/images/2017/09/arton8576-4c284-5b33b.jpg)

![](/images/2017/09/arton5556-25861-8a405.jpg)
