---
title: "Construire l’autonomie"
date: 2017-09-19T09:33:46Z
draft: false
image: "2017/09/06a02ac8f83ebad05ecf792e7e9779.jpg"
description: "Retrouvons-nous le 20 septembre au CSA d'Ivry pour construire l’autonomie au-delà du calendrier syndicale."
---

Pendant l’Interlude Interlutte qui a eu lieu mercredi dernier, s’est exprimée la volonté commune de se retrouver prochainement pour construire l’autonomie au-delà du calendrier syndicale.

L’occupation de lieux d’organisations est toujours un moment fort des luttes. Il faut faire vivre ces lieux, ce sont les points de départ de notre autonomie, de la construction commune d’autres formes d’existences, émancipées de la domination.
La lutte tire sa force de ces solidarités concrètes. Nous vous invitons donc à venir mettre cette perspective en pratique.

Rendez-vous le 20 septembre à 18 heures au Centre Social Autogéré d’Ivry (37 rue Marceau - Ligne 7 Pierre et Marie Curie)

Pour construire l’autonomie

