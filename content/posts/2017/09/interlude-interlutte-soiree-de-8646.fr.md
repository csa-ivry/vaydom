---
title: "Interlude Interlutte - Soirée de convergence des luttes à Ivry"
date: 2017-09-16T12:13:25Z
draft: false
image: "2017/09/485c7e2e8a1bedd8c9cf4c644d83f4.jpg"
description: "Compte rendu et discussion ouverte sur la manif du 12 septembre 2017 et la suite du mouvement Jam Session et Cantine festive à prix libre."
---

Compte rendu et discussion ouverte sur la manif du 12 septembre et la suite du mouvement Jam Session et Cantine festive à prix libre. Au Centre Social Autogéré d’Ivry (37 rue Marceau - Ligne 7 Pierre et Marie Curie.)

![](/images/2017/09/thumbnail_affiche_interlude_interlutte2-01-7cbd6.jpg)

### A toutes celles et ceux qui luttent,

Contre celles et ceux qui nous oppressent, nous exploitent et qui prétendent nous représenter.

Quels que soient vos modes d’organisation, quels que soient vos expériences et votre vécu.

RENDEZ VOUS LE 13 SEPTEMBRE A 18H au Centre Social Autogéré d’Ivry (37 rue Marceau - Ligne 7 Pierre et Marie Curie.)
Pour une soirée de convergence des luttes.

Après la manifestation, retrouvons nous ensemble, pour échanger sur celle-ci, et sur les actions à venir.

Au programme, une discussions sur la manifestation de la veille, la convergence des luttes, et la lutte en général. Une Jam Session où chacun.e est invité.e à exprimer son art. Et une bouffe vegan à prix libre pour échanger et rire autour d’un bon repas.

Il est nécessaire de se mettre en contact, de créer un lien entre les différents cortèges, de la tête a la queue, nous avons un vécu en commun, des objectifs en commun, des sensibilités en commun. De la violence à la non violence, nous allons dans la même direction.

Dans les mois qui viennent les cortèges doivent être solidaires,nous devons nous serrer les coudes. Il faut qu’on se rapproche les un.e.s des autres. Ne mettons pas nos différences de cotés, cultivons leur complémentarité.
