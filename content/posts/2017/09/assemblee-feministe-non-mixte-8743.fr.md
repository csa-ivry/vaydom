---
title: "Assemblée féministe - non mixte meufs et minorités de genre"
date: 2017-09-22T21:49:47Z
draft: false
image: "2017/09/35c07a3ecce2c60fa210cae51c0ef1.png"
description: "Assembleé féministe de luttes le samedi 30 septembre 2017 au CSA d'Ivry."
---

Le Samedi 30 septembre à 14h, retrouvons nous pour envisager les perspectives de lutte et de mobilisation lors de la prochaine AG au CSA (Centre Social Autogéré) d’Ivry, 37 rue Marceau à Ivry-sur-Seine, Metro 7 – Pierre et Mairie Curie, Tram 3 – Maryse Bastié

À l’occasion de la prochaine AG nous pourrons continuer à nous organiser et discuter autour des thématiques des violences, du travail et d’autres qui ressortiront des débats, en partant des conditions de vie et des besoins de chacunE, ainsi que des perspectives d’une prochaine manifestation autonome le 8 mars 2018.

![](/images/2017/09/21462795_895570630584163_2858692125780598415_n-2-2a365-58987.png)
