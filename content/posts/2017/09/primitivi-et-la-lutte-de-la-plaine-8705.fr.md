---
title: " Primitivi et la lutte de la Plaine au CSA d’Ivry"
date: 2017-09-18T09:53:01Z
draft: false
image: "2017/09/a0507cc66aa84e3279c5475cb2eee6.jpg"
description: "Soirée projection débat avec le collectif Primitivi de Marseille autour de la lutte contre la rénovation urbaine de nos quartiers"
---

Soirée projection débat avec le collectif Primitivi de Marseille autour de la lutte contre la rénovation urbaine de nos quartiers

Jeudi 21 septembre à 19h au Vaydom, le CSA d’Ivry, Primitivi nous présentera « La bataille de la Plaine », un [documentaire-fiction utopique](https://www.doctoratsauvage.primitivi.org/experimentations) à partir de la proclamation la Commune Libre de la Plaine, suivi de quelques-unes de leurs [chroniques](https://vimeo.com/primitivi) sur ce quartier marseillais.

Les [habitant.e.s de la Plaine luttent](http://www.primitivi.org/spip.php?article727) depuis longtemps contre la rénovation urbaine, en se réappropriant des rues et des places car « c’est le quartier lui même qui sait ce qu’il veut et qui fait ce qu’il veut ».

Chaque année, on est nombreux et nombreuses à s’y retrouver pour le carnaval indépendant de la Plaine, événement rebelle et festif qui participe à cette résistance.

![](/images/2017/09/images-31-82042-c1170.jpg)

A Ivry aussi, on fait face à des projets de rénovation urbaine comme Ivry Confluences. A Ivry aussi, on s’y oppose en occupant les lieux de diverses façons. L’année dernière a eu lieu un premier carnaval sauvage.

Que ce soit à Marseille, Ivry ou ailleurs, nous luttons contre des projets d’aménagement avec l’envie de vivre différemment dans la ville.

Retrouvons nous ce jeudi soir pour échanger avec Primitivi autour de la réappropriation populaire de nos quartiers !

### P.-S.

Primitivi est « une téloche de rue marseillaise qui va bientôt fêter ses 20 piges. Du côté des luttes sociales et politiques depuis le début, on fabrique peu à peu une archive de la Marseille rebelle et on bricole avec nos compagnon.e.s du bitume un récit populaire. »
http://www.primitivi.org/

Le projet la bataille de la Plaine : https://www.doctoratsauvage.primitivi.org/experimentations

Le Vaydom, centre social autogéré d’Ivry, est un lieu occupé d’habitation et d’activités qui est ouvert depuis mai 2017.
https://paris-luttes.info/il-semblerait-qu-on-aie-un-nouvel-8642
