---
title: " Rencontre avec des membres de la Schwarz-Rote Hilfe (Secours noir et rouge)"
date: 2017-11-19T12:24:26Z
draft: false
image: "2017/11/5d7506b9f64f351f7f8ee2cc5e75c8.png"
description: "Dimanche 19 novembre à 17h, rencontre avec des membres de la Schwarz-Rote Hilfe (le secours noir et rouge), un groupe anti-répression de la ville de Münster (Allemagne) indépendant de la Rote Hilfe."
---

Le dimanche 19 novembre à 17h aura lieu, au centre social autogéré d’Ivry, une rencontre avec des membres de la Schwarz-Rote Hilfe (le secours noir et rouge), un groupe anti-répression de la ville de Münster (Allemagne) indépendant de la Rote Hilfe.

![](/images/2017/11/arton9072-29141.png)

Depuis plusieurs années, ce groupe assure un travail d’information, de prévention, de soutien et de solidarité financière face à la répression.

Il profiteront de leur venue pour, après une brève vidéo d’introduction, nous parler de l’histoire du travail anti-répression en Allemagne, puis nous présenter plus concrètement leur travail :

- de prévention
- de EA ( la légal team )
- de soutient des militant*es touché*es par la répression

Cette rencontre est destinée à être un moment d’échange, de questions et de partage d’expériences. Elle vise également à resserrer les liens au-delà des frontières, face à une répression qui tend à s’uniformiser à échelle européenne.

Le lieu organise tous les dimanche une cantine populaire à 20h, et ce dimanche ne fait pas exception ! Nous pourrons donc en profiter ensemble.

Site de la Schwarz-Rote Hilfe : http://schwarzrotehilfe.blogsport.de/</a>
