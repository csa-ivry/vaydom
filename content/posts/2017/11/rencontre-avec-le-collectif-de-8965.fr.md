---
title: "Projection et rencontre avec le collectif de soutien à Oury Jalloh au CSA d’Ivry"
date: 2017-11-01T10:33:55Z
draft: false
image: "2017/11/0761221cf3ed40627e57110708c399.jpg"
description: "Projection et rencontre avec le collectif de soutien à Oury Jalloh, mort brûlé dans un commissariat de Dessau (Allemagne)."
---

Projection et rencontre avec le collectif de soutien à Oury Jalloh, mort brûlé dans un commissariat de Dessau (Allemagne). Moment de solidarité internationale au Centre Social Autogéré « Vaydom » d’Ivry-sur-Seine.

Oury Jalloh avait 23 ans quand il est mort.

Ayant fui la guerre civile, Oury est venu demander l’asile politique en Allemagne.

Débouté de l’asile, il est interpellé par la police de Dessau le 7 janvier 2005 en état d’ivresse et placé dans la cellule n°5 située au sous-sol du commissariat de Rosslau.

Attaché à un matelas, il est retrouvé le lendemain mort, brûlé dans sa cellule.

Très rapidement, la police et la justice s’accordent sur une version : Oury Jalloh se serait suicidé.

Des [expertises indépendantes](https://vimeo.com/79113052) ont démontré qu’en l’absence de matière combustible, Oury Jalloh n’aurait pas pu se suicider de cette manière.

Malgré les incohérences de la version policière, les deux policiers incriminés sont relaxés en 2008. La décision de relaxe a été annulée pour l’un des deux officiers, qui a été condamné en décembre 2012 à 10 000 euros d’amende pour homicide involontaire (« par inadvertance »).

Pour autant, la Justice allemande a confirmé la thèse du suicide par une décision rendue en 2014. En octobre 2017, le parquet de Halle a statué définitivement sur cette version en estimant qu’aucune autre version plausible n’était à attendre.

L’initiative en mémoire de Oury Jalloh se bat depuis plusieurs années pour obtenir la vérité dans cette affaire et mobiliser autour des violences policières. Une conférence de presse est prévue sur la place de la République à Paris ce vendredi 3 novembre à 15h30.

Le collectif invite également celles et ceux qui le souhaitent à les rencontrer autour de la projection du film « Mort en cellule », le vendredi 3 novembre à 18 heures au Centre Social Autogéré d’Ivry, situé 37 rue Marceau à Ivry sur Seine (métro 7 Pierre et Marie Curie, tramway 3a « Maryse Bastié »)

### P.-S.

La photo du logo est un portrait d’un acteur du film [Oury Jalloh](https://vimeo.com/35945624)
