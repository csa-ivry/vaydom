---
title: "Assez d’expulsions, assez de logements vides !"
date: 2017-11-23T06:47:41Z
draft: false
image: "2017/11/8af05dfc739a67d06edb23f205d6df.jpg"
description: "Invitation à une assemblée autour du problème du logement : relions nos résistances, partageons nos savoirs et relançons les luttes le 6 décembre à 19h au CSA Vaydom d’Ivry."
---

Invitation à une assemblée autour du problème du logement : relions nos résistances, partageons nos savoirs et relançons les luttes le 6 décembre à 19h au CSA Vaydom d’Ivry (37 Rue Marceau).

![](/images/2017/11/d0df4a21f8992bc8958b41973ef62d.jpg)

Invitation à une assemblée autour du problème du logement : relions nos résistances, partageons nos savoirs et relançons les luttes.

Il y a une question récurrente qui nous pousse à parler et partager des discussion politiques : comment le mouvement peut-il se donner une continuité et une articulation dans le temps ? Qu’est-ce qu’il reste du cortège de tête après le cortège de tête ? C’est-à-dire, qu’est-ce qu’il reste de ce moment d’autonomie expérimentée collectivement ? Quelle signification même donner au mot « autonomie » aujourd’hui, c’est-à-dire par quelles pratiques on continue à produire de l’autonomie même au delà des manifestations.

Depuis quelques semaines, des discussions ont lieu au CSA Vaydom autour de la question de l’autonomie. À partir des ces discussions, on a commencé a mettre au centre les questions qui sont centrales dans nos vies et qui déterminent d’hors et déjà des moments de résistance. En particulier on a identifié comme central le problème du logement.

La gestion des logements est un vecteur puissant d’accumulation et d’organisation du territoire par le patronat et l’état, et les conséquences sont écrasante : 2,6 milions de logements vacants, 4 milions de personnes ne sont pas ou mal logé, avec ce dernier nombre qui a augmenté de 50% en 10 ans. En 2015, 14 mille expulsions locatives avec le concours de la force publique,  nombre aussi en augmentation (+24% en 2015).

En même temps plusieurs résistances sont déjà en place : des gens occupent des logements vide, des squats, ou tout simplement se débrouillent pour ne pas payer un loyer (ou le payer moins cher).

C’est à partir de ces constatation qu’on voudrait proposer un pari, faire l’essai d’une modalité commune de recherche, d’analyse et d’action autour de cette problématique.

Plusieurs ateliers et forme de solidarité existent déjà. Beaucoup de formes de lutte sont de fait actives dans la ville, par différents sujets. Ce pari serait donc celui de créer une cartographie du problème-logement, de mettre en lien ceux qui luttent contre les expulsions et les évictions, de construire une analyse théorique et en même temps pratique qui puisse être un outil pour comprendre les enjeux de la rénovation urbaine parisienne. De multiplier les ateliers pratiques et juridiques qui permettent de collectiviser des savoirs de lutte.

C’est pour répondre à ces questions que nous vous invitons à venir le 6 décembre à 19h au CSA Vaydom d’Ivry (37 Rue Marceau).

L’essai de cette modalité de rencontre et de réunion ne concerne pas seulement la thématique du logement. C’est aussi une manière différente de concevoir nos partages politiques. On voudrait traiter un problème qui touche notre territoire et nos vies, produire des affinités par des objectifs communs qu’on décidera de partager.

Mettons au centre les besoins et les diverses forme de résistance qui sont déjà là. Organisons politiquement l’incompatibilité concrète de la vie avec le capitalisme. Construisons un nouveau rapport de force.
