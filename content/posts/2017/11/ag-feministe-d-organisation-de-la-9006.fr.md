---
title: "AG féministe d’organisation de la Marche de Nuit du 25/11 à Paris"
date: 2017-11-13T09:07:42Z
draft: false
image: "2017/11/f90deb65a8ef1153447b932b040e77.png"
description: "AG féministe exceptionnelle le dimanche 12 novembre à 15h au Vaydom pour organiser la Marche de Nuit du 25 novembre à Paris."
---

### AG féministe exceptionnelle le dimanche 12 novembre à 15h au Vaydom pour organiser la Marche de Nuit du 25 novembre à Paris.

L’AG féministe ouverte et intersectionnelle organisatrice de la marche du 8 mars poursuit ses réunions. L’AG féministe organisatrice du 8 mars 2017 s’appelle désormais l’AG féministe en mixité choisie.

"Notre vision du féminisme se fonde sur la reconnaissance de la totale légitimité des premièrEs concernéEs à décider de leurs stratégies de lutte contre les violences et discriminations qu’elles subissent.

Organisons-nous pour nous défendre, au cœur des luttes des plus stigmatisées, réprimées, et précaires d’entre nous !"

### Ordre du jour :

L’Assemblée féministe en mixité choisie propose une AG exceptionnelle le dimanche 12 novembre à 15h au au Vaydom - CSA Centre Social Autogéré d’Ivry 37 rue Marceau Ivry-sur-Seine (94) Métro Pierre-et-Mairie Curie Tram-T3 Maryse-Bastié pour organiser une Marche de Nuit le 25 novembre à Paris.

### Charte :

L’AG a décidé de formaliser son fonctionnement, ses principes et pratiques (charte) c’est-à-dire de les définir et de les rendre publique afin de rassembler et de construire un espace de lutte accessible à touTEs.

Parmi ses fonctionnements, principes et pratiques nous avons commencé par là et nous proposons lors de la prochaine AG de continuer :

- L’ AG est ouverte aux militantEs et non militantEs féministEs.
- L’AG s’organise en mixité choisie sans hommes cis.
- L’AG est libre de s’organiser en non mixités.
- L’AG soutient l’autodéfense collective.

Toutes les initiatives mixtes de genre excluent les mecs cis agresseurs.

### Fonctionnement de la liste mail :

TouTEs les personnes participant à l’AG sont libre de s’inscrire à la liste mail de l’AG si elleux le souhaitent pendant l’AG une feuille tournera pour que vous puissiez vous inscrire.

Cette liste mail est une liste de diffusion ce n’est pas une liste mail de discussion, de débat ou prise de décision.
La liste mail est modérée à postériori de l’envoie de mail par 4 administrateurices qui se donnent les moyens d’être réactif-ves. L’AG donne mandat aux administrateurices de se concerter et de prendre l’initiative de bloquer les personnes tenant des propos problématiques d’informer les personne par mail qu’elles ont été bloquées de rappeler la charte et de faire un compte rendu en AG.

### Fonctionnement de la communication :

Un groupe de personnes se chargent de faire la communication c’est-à-dire de poster l’appel à la prochaine AG sur des sites d’informations et sur les réseaux sociaux.

### Fonctionnement de la recherche de salle :

Un groupe de personnes se chargent de trouver une salle accessible.

### Proposition d’ordre du jour :

L’ordre du jour est ouvert à toutes nouvelles propositions en AG.

### P.-S.

Rendez vous dimanche 12 novembre à 15h au Vaydom- CSA Centre Social Autogéré d’Ivry 37 rue Marceau Ivry-sur-Seine (94) Métro Pierre-et-Mairie Curie Tram-T3 Maryse-Bastié

