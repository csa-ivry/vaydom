---
title: "Visio-conférence avec Kamel Daoudi, assigné à résidence depuis 9 ans"
date: 2017-11-11T11:13:45Z
draft: false
image: "2017/11/89d3bf431f812b315bcfc9eaa87c0d.jpg"
description: "Visioconférence avec Kamel Daoudi le 17 novembre 2017 à 19 heures, afin d’échanger sur la façon dont l’État instrumentalise la lutte contre le terrorisme."
---

Une visioconférence est organisée avec Kamel Daoudi ce 17 novembre 2017 à 19 heures au Centre Social Autogéré d’Ivry, afin d’échanger sur la façon dont l’État instrumentalise la lutte contre le terrorisme pour créer un nouveau paradigme lui permettant de contrôler et d’asservir davantage les populations dans un système de domination capitaliste intégré.

#### Sortir de l’état d’urgence, critiquer l’anti-terrorisme, combattre les états d’exception.

![](/images/2017/11/antiterro-2-5489f.jpg)

Après deux ans sous état d’urgence, l’État français vient d’adopter la nouvelle loi antiterroriste qui va pérenniser la plupart des mesures prévues par l’état d’urgence en les versant dans le droit commun.

L’anti terrorisme est un vieil outil de domination. Chaque attaque à main armée sert de prétexte à un serrage de boulon et l’étau se resserre chaque jour un peu plus autour de nos libertés, de notre liberté.

Il peut être utile de revenir plus de 15 ans en arrière, lorsqu’en 2001 les États-Unis unissaient le monde entier autour d’une chasse effrénée aux musulmans, invitant chaque pays à leur livrer ses propres djihadistes. Souvenez-vous Guantánamo…

Kamel Daoudi a été interpellé en Angleterre fin 2001, soupçonné de liens avec le terrorisme dans une enquête autour d’un projet d’attentat contre l’ambassade des États-Unis à Paris. Ce projet d’attentat n’ayant jamais pu être démontré par la justice française, seule l’association de malfaiteurs en relation avec une entreprise terroriste a été retenue contre Kamel. Condamné à 9 ans de prison ferme en première instance, sa peine a été abaissée à 6 ans de prison en appel, condamnation assortie d’une interdiction définitive du territoire et d’une déchéance de la nationalité française. Victime de violences carcérales commises par des agents de l’AP, il a été sanctionné par des peines complémentaires. A sa sortie de prison en 2008 après six ans et demi de détention, il a été assigné à résidence dans l’attente de sa reconduite à la frontière. Cela fait donc 9 ans qu’il est assigné à résidence dans différents villages du pays (Aubusson, Longeau-Percey,  Fayl-Billot, Lacaune, Carmaux, Saint-Jean d’Angély), logé dans un hôtel, séparé de sa famille et contraint de pointer trois fois par jour à la gendarmerie.

Le Centre Social Autogéré d’Ivry vous invite à une visio-conférence et un débat avec Kamel Daoudi le vendredi 17 novembre 2017 à 19h00 au 37 rue Marceau, 94 200 Ivry sur Seine - Métro 7 « Pierre et Marie Curie » ou Tramway 3a « Maryse Bastié »
