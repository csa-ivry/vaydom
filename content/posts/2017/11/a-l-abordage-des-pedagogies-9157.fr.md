---
title: "A l’abordage des pédagogies alternatives !"
date: 2017-11-29T11:10:21Z
draft: false
image: "2017/11/027a7f27315a5d99bc8f6e78ed861d.jpg"
description: "Soirée de présentation et discussion sur des pédagogies alternatives au CSA d’Ivry"
---

### Soirée de présentation et discussion sur des pédagogies alternatives au CSA d’Ivry

![](/images/2017/11/arton9157-bb840.jpg)

18h / Présentation de différents courants de pédagogie avec Romain G. pirate de l’éducation

nationale et membre du GFEN.
19h30 / Repas, à prix libre

20h30 / Projection du film « Une journée dans la classe de Sophie. A la rencontre du 3e type en éducation prioritaire » de Claire Lebrun

suivie d’une discussion avec la réalisatrice
