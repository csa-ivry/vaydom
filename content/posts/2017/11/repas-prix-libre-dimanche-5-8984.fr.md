---
title: "Repas prix libre - Dimanche 5 novembre - CSA d’Ivry - 20h"
date: 2017-11-03T12:49:52Z
draft: false
image: "2017/11/2c8d684193e2c92552234eb872d1f8.jpg"
description: "Repas de soutien au CSA d’Ivry par le Groupe Anarchiste Alhambra"
---

Repas de soutien au CSA d’Ivry par le Groupe Anarchiste Alhambra

![](/images/2017/11/23146425_10156288274849769_1240325100_n-05960-25b4c.jpg)

Le Groupe Anarchiste Alhambra vous invite ce dimanche à un repas à prix libre au Centre Social Autogéré d’Ivry à partir de 20h ! Ce sera l’occasion de se retrouver pour un moment collectif en ces temps de luttes sociales éprouvantes.

Pour venir : métro 7 Pierre et Marie Curie ou tram 3 Maryse Bastié.

Adresse : 37 rue Marceau à Ivry-sur-Seine
