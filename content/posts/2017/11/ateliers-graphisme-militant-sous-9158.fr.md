---
title: "Ateliers graphisme militant sous logiciels Libres, au Csa d’Ivry !"
date: 2017-11-30T09:35:09Z
draft: false
image: "2017/11/b157949dd4743121ebaa5e272507f3.jpg"
description: "Lancement de la session d’ateliers graphisme militant, en logiciels Libres (et gratuits) au CSA Vaydom."
---

### Lancement de la session d’ateliers graphisme militant, en logiciels Libres (et gratuits) au CSA Vaydom.

L’idée c’est :

1. Des ateliers graphisme militant, sous logiciels Libres : Gimp, Inkscape, Scribus, etc. pour se libérer des logiciels propriétaires payants de la suite Adobe et autres. Et pourquoi pas, partager ses travaux librement, si on le souhaite. Philosophie Copyleft ([1](https://fr.wikipedia.org/wiki/Copyleft) [2](https://www.gnu.org/licenses/copyleft.fr.html)). Exemples de licences créatives libres ([Art libre]http://artlibre.org) [Creative Commons](http://creativecommons.fr)).
2. S’initier ou se perfectionner aux outils de communication imprimée et/ou web.
3. Atteindre une autonomie technique pour faire ses affiches, brochures, logos, sites web, etc pour son orga, son asso, un projet militant, une envie qui vous tient à cœur... Débutant-e, confirmé-e, pro... tout le monde est bienvenu !

### Programme de la 1re séance :

Une partie échanges, discussions :

- faire connaissance
- exposer ses besoins, envies et attentes par rapport aux ateliers graphisme
- et pourquoi pas : des synergies projets entre nous
- suivant ces éléments, on co-élabore le programme sur plusieurs semaines/mois

Une partie pratique :

- installation des logiciels pour celles/ceux qui ne l’auraient pas déjà fait
- initiation : présentation des interfaces, premiers pas ou plus si tout le monde est déjà initié...

### Logistique, dans l’idéal... :

- De préférence, amener son ordi portable, bien que nous ayons des ordis à disposition mais peu puissants.
- Connexion internet : amener son câble RJ45, sinon y aura peut-être du wifi.
- Si vous ne venez pas avec votre ordi, amener un moyen de stockage tel qu’une clé usb pour apporter vos fichiers graphiques avec vous.
- Idéalement, vous pouvez déjà noter/crobarder des idées en amont et les amener le jour J.
- Si vous savez le faire, le mieux est d’installer les logiciels Libres qui vous intéressent avant l’atelier, histoire de gagner un peu de temps. Sinon pas de panique on fera ça sur place.
- Amener le logo de votre groupe, orga, asso..., des fontes (typos), des photos ou toutes autres matières visuelles que vous souhaiteriez utiliser dans vos projets graphiques.

Pour les photos, le plus savoureux est d’avoir les siennes, sinon vous pouvez vous alimenter sur ces sites, par exemples :

<a href="https://pixabay.com">https://pixabay.com</a> - <a href="https://www.flickr.com/creativecommons">https://www.flickr.com/creativecommons</a> (filtrer la licence sur une où il y a droit de modifs)

### Quand ?

Vendredi 8 décembre 2017. Ça commence à 17h pour une séance d’environ 3h.

### Où ?

Au CSA Vaydom à 5 minutes de la station de tram Maryse Bastier (ligne T3)

Adresse : 37 rue Marceau - Ivry sur Seine

CSA Vaydom / La LibreRie solidaire
