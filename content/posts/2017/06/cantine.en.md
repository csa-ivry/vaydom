---
title: "Open doors and popular kitchen at the squatted social center"
date: 2017-07-01T01:03:47+02:00
draft: false
image: "2017/06/csa-photo-mini.jpg"
tags: ["Announcements"]
description: "Tous les dimanches à partir de 16h venez découvrir le Centre Social Autogéré, les gens qui l’animent et l’habitent. A partir de 20h, venez partager un bon repas ensemble à la cantine populaire ouverte sur le quartier."
---

Tous les dimanches à partir de 16h venez découvrir le Centre Social Autogéré, les gens qui l’animent et l’habitent. A partir de 20h, venez partager un bon repas ensemble à la cantine populaire ouverte sur le quartier.

Depuis un mois déjà un nouveau lieu est occupé à Ivry-sur-Seine pour y faire vivre un Centre Social Autogéré, au 37 rue Marceau.

Laissé vide depuis presque deux ans, le bâtiment est réquisitionné par des dizaines de mal logé.e.s, des familles, des précaires, des sans-papiers.

En plus d’être un lieu d’habitation, cet espace se veut un lieu où expérimenter des nouvelles formes d’entraide et d’auto-organisation et lutter contre toute forme de domination.

Des permanences d’accès aux droits et des cours de langues existent déjà. D’autres activités ouvertes sur le quartier commencent à démarrer : projections de films, cantines, AG, débats, ateliers de théâtre… tout le reste est encore à inventer !

Le dimanche c’est le moment des portes ouvertes : le CSA vous ouvre son portail de 14h à 18h !

Venez nous rencontrer pour connaitre le Centre Social Autogéré, les gens qui l’animent et l’habitent, vous informer et participer aux activités, papoter et débattre…

Ensuite, comme tous les dimanches, on mangera un bon repas en compagnie à la cantine populaire ouverte sur le quartier à partir de 19h30. La cantine est à prix libre et autogérée : vous êtes tou-te-s les bienvenu-e-s pour venir donner un coup de main !

Face à la menace d’expulsion qui incombe ces jours-ci sur plusieurs lieux d’habitations et d’activités en Ile-de-France (le CSA l’Attieké à Saint-Denis, Les Baras à Bagnolet…), soyons solidaires et continuons à construire des espaces libérés !

![Photo façade CSA d'Ivry](/images/2017/06/paris-luttes.infocsa-7a98a-d4ae0-f6f6b40167b4e9ac759f88549e6edfc5b66928d7.jpg)

EDIT 19 Juillet 2017: Corrections factuelles, orthographiques et mise à jour des rendez-vous.
