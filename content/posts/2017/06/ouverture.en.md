---
title: "Opening meal at the squatted social center"
date: 2017-06-08T01:20:47+02:00
draft: false
tags: ["Announcements"]
image: "2017/06/flyer-ouverture-featured.jpg"
description: "Ce dimanche 11 juin, venez nous rencontrer au Centre Social Autogéré (CSA) d’Ivry : ouverture à partir de 14h et repas à partir de 21h45 !"
---

Ce dimanche 11 juin, venez nous rencontrer au Centre Social Autogéré (CSA) d’Ivry : ouverture à partir de 14h et repas à partir de 21h45 !

Depuis plusieurs jours un nouveau lieu est occupé à Ivry-sur-Seine pour y faire vivre un Centre Social Autogéré, au 37 rue Marceau.

Laissé vide depuis presque deux ans, le bâtiment administré par Nexity (grand et méchant promoteur immobilier) est réquisitionné par des dizaines de mal logé.e.s, des familles, des précaires, des sans-papiers.

En plus d’être un lieu d’habitation, nous voulons un espace où expérimenter des nouvelles formes d’entraide et d’auto-organisation et lutter contre toute forme de domination.

Des permanences d’accès aux droits et des cours de langues existent déjà, d’autres activités ouvertes sur le quartier prendront forme bientôt : projections de films, cantines, débats…le reste est à inventer !

Venez donc nous rencontrer pour faire vivre le lieu et proposer des nouvelles activités chaque dimanche, de 16 à 20 heures.

Et pour commencer l’aventure, ce dimanche 11 juin on ouvre les portes sur le quartier pour la première fois ! Les habitant.e.s vous invitent à partager un bon repas à partir de 21h45, pour rompre le jeûne tou.te.s ensemble, tchatcher autour d’un thé et se connaître.


37 rue Marceau

Metro 7 – Pierre et Mairie Curie, Tram 3 – Maryse Bastié

Initialement publié sur [paris-luttes.info](https://paris-luttes.info/repas-d-ouverture-du-nouveau-8315).

EDIT 19 juillet 2017: Corrections factuelles et mise à jour des rendez-vous.
