---
title: "Projection de « Attiéké all stars » et débat"
date: 2017-10-17T22:32:24Z
draft: false
image: "2017/10/90e0e1a8c6e1cbe0e39436cc3df835.png"
description: "Présentation du film « Attiéké all stars » et débat autour du CSA de Saint-Denis, en présence du réalisateur. Jeudi 19 octobre à 20h, au Centre social Autogéré d’Ivry."
---

Présentation du film « Attiéké all stars » en présence du réalisateur, suivi d’un débat le jeudi 19 octobre à 20h, au Centre social Autogéré d’Ivry.

![](/images/2017/10/arton8883-44614.png)

Extrait de la note de présentation du film :

L’Attiéké est un véritable radeau voguant dans une ville de banlieue parisienne en pleine rénovation urbaine, qui au-delà de tenir tête à une fédération sportive, fait la nique aux promoteurs immobiliers, aux marchands de sommeil et taquine la mairie communiste de Saint Denis. Étudiants précaires, travailleurs et chômeurs avec ou sans papiers, réfugiés et exilés, tous ont pris acte du déni qui pèse sur eux et s’organisent dans cette bâtisse atypique, colorée et imposante. L’Attiéké est un carrefour où s’esquisse le portrait d’une frange invisible de la société, sans cesse dénigrée ou ignorée.

Venez regarder le film et discuter avec le réalisateur et les Csaeuses-eux du Vaydom sur l’auto-gestion, la réappropriation des espaces et les différentes formes de luttes qu’on peut mener à partir des Centres sociaux auto-gérés.
