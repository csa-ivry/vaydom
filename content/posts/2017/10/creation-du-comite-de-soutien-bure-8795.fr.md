---
title: "Création du Comité de soutien Bure Paris IDF"
date: 2017-10-19T09:55:05Z
draft: false
image: "2017/10/8460eb3c1377a7d4887958cbc3952b.jpg"
description: "Dans la Meuse, l’État cherche depuis plus de 20 ans à creuser sa poubelle atomique. Création d'un comité de soutien aux opposant-e-s. Ni à Bure, ni ailleurs !"
---

Première AG le jeudi 5 octobre, au Centre social autogéré d’Ivry à 19h - réunions récurrentes toutes les 2 semaines

## ANDRA dégage ! Ni à BURE ni AILLEURS !

![](/images/2017/10/img_3754sm-eb39a.jpg) 

Dans la Meuse, l’État cherche depuis plus de 20 ans à creuser sa poubelle atomique à coups d’intimidations, d’achats des consciences et de colonisation des territoires de toute la filière de l’industrie nucléaire.

Récemment, la répression sur les opposants a passé un nouveau cap. Après les brutalités commises lors des dernières manifestations, la surveillance policière permanente des habitant.e.s, le 20 septembre des perquisitions à grand spectacle ont eu lieu dans des habitations privées ou collectives, dont la maison de résistance Bure Zone Libre.

Ce dernier coup d’éclat n’a fait que renforcer la détermination de toutes celles et ceux qui luttent pour qu’on enterre au plus vite le projet de stockage souterrain CIGEO fomenté par l’Agence de gestion des déchets radioactifs, l’ANDRA.

Les vautours du nucléaire semblent jouer leur dernière carte, celle des flics et des juges, car tout vacille autour du projet CIGEO. Depuis juin 2016, une forêt (le bois Lejuc) a été libérée de l’emprise de l’ANDRA, qui cherchait à y creuser ses premiers puits, et un mur de béton a été abattu par les opposants. Les « hiboux », résistant.e.s qui vivent avec les arbres s’apprêtent à passer leur deuxième hiver dans la forêt. D’autant que l’ANDRA se prend des revers sur tous les terrains : plusieurs défaites retentissantes devant les tribunaux, la sécurité de CIGEO recadrée méchamment par l’autorité de sûreté nucléaire, et la découverte d’un site archéologique qui risque de retarder encore les travaux...

Afin de retarder sa chute, l’industrie nucléaire a besoin de dire qu’elle a trouvé une solution aux plus toxiques des déchets radioactifs : les enterrer à 500 m sous terre à Bure. La répression contre les militant.e.s et les habitant.e.s autour de CIGEO n’est qu’un prétexte pour disqualifier toute remise en cause des fondements mêmes de l’énergie nucléaire.

### Solidarité aux habitant.e.s de Bure et des environs ! Soutien aux habitant.e.s du bois Lejuc !

Le comité de soutien Paris IDF sera présent à la première rencontre inter-comités prévue à Bure très bientôt (détails sur [vmc.camp](https://vmc.camp)).

Rejoignez le comité de soutien Paris ile-de-France:

- Contact : comitebureparisidf(AT)riseup.net AG inaugurale le jeudi 5 octobre 2017 à 19h.
- Lieu : Vaydom (Centre social autogéré), 37 rue Marceau, 94 Ivry sur Seine (T3a Maryse Bastié, M7 Pierre et Marie Curie)
- Prochains rendez-vous : même heure même lieu les jeudi toutes les 2 semaines.
- TRACT à imprimer et à diffuser dans le post-scriptum ci-dessous

+ Soutien financier à la lutte : https://www.helloasso.com/associations/les-amis-du-bocage/collectes/ils-saisissent-la-solidarite-remplace

AGENDA des prochaines semaines :

- 22 octobre : journée de renforcement de l’occupation, construction d’une cabane solidaire dans le bois Lejuc.
- 24 octobre : rassemblement à Bar-le-Duc pour le délibéré du procès de Jean-Pierre Simon & soutien à un autre ami paysan qui passe au tribunal...
- 28 octobre : marché paysan à Bure. Pour plus d’infos, consultez [vmc.camp](https://vmc.camp) et [burestop.eu](http://www.burestop.eu)


![](/images/2017/10/bure-paris-tract-oct17-719e4-78de0.jpg)
