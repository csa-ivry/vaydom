---
title: "AG féministe en mixité choisie"
date: 2017-10-09T11:09:54Z
draft: false
image: "2017/10/d0d8279f1a5e2fba047018ba239231.png"
description: "Assembleé féministe ouverte à tou.te.s sauf mecs cisgenres, samedi 4 novembre à 14h au CSA d'Ivry."
---

![](/images/2017/10/arton8824-377b8.png)

L’AG féministe organisatrice du 8 mars 2017 s’appelle désormais l’AG féministe en mixité choisie. Elle est une assemblée féministe inclusive, intersectionnelle et se réunit en non-mixité meufs et minorités de genre, autrement dit elle est ouverte à tou.te.s sauf mecs cisgenres.

Vous pouvez lire la charte de l’AG sur [la page facebook](https://www.facebook.com/AGfeministe8mars2017/)

Nous appelons à une nouvelle AG le samedi 4 novembre à 14h, pour envisager les perspectives de lutte et de mobilisation. Cette fois, et comme décidé à la dernière AG, il sera prévu des temps pendant l’AG pour que des groupes de travail se réunissent. Il a été convenu de deux groupes :

- Genre et travail  (et notamment sur les liens avec le mouvement social contre la réforme du code du travail)
- Violences sexistes dans les milieux militants 

Ces deux groupes sont ouverts à tou.te.s celleux qui souhaitent y participer, il suffit d’être présent.e à l’AG ce jour-là.
L’AG sera suivie d’un cours d’autodéfense inspiré du Kobudo (art martial d’Okinawa avec Bô, bâton long), à prix libre. 

Venez avec votre bâton !
