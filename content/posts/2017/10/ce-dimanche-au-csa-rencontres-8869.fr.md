---
title: "Ce dimanche au CSA d’Ivry : rencontres autour du 17 octobre 1961"
date: 2017-10-13T21:25:27Z
draft: false
image: "2017/10/8fd25d42eb109638c07345d5e18080.jpg"
description: "Dimanche 15 octobre, journée de commémoration du massacre raciste et colonial du 17 octobre 1961 perpétré par l'État français contre les Algérien-ne-s."
---

![](/images/2017/10/arton8869-83ac5.jpg)

## Le 17 octobre 1961, c’est quoi ?

Luttant pour l’indépendance de l’Algérie vis-à-vis de la République coloniale française, des Algérien-ne-s s’organisent et manifestent. Depuis le 5 octobre 1961, un couvre-feu interdit la circulation aux personnes algériennes dans les rues de Paris et des banlieues alentours. Le 17 octobre une manifestation est organisée par le Front de Libération Nationale (FLN) parisien, pour boycotter le couvre-feu. Les algérien-ne-s étaient appelé-e-s à venir défiler sans armes et en famille.

Le 17 octobre, la police est à cran, car depuis plusieurs semaines à Paris et en banlieue, des attaques menées par les résistant-e-s algérien-ne-s ciblant la police ont fait plusieurs morts dans leur rang. Sous la pression des flics, le préfet de Paris Maurice Papaon leur a garanti l’impunité s’ils font usage de leurs armes contre des algérien-ne-s.
Bastonnades, tortures, assassinats... S’ensuit une chasse aux Algérien-ne-s qui dure plusieurs jours et fait un grand nombre de morts (entre plusieurs dizaines et quelques centaines). L’État tentera en vain de dissimuler les faits.

- 14h : Ouverture des portes
- 14h30-16h30 :Théâtre-forum sur les violences policières
Mises en situation par le théâtre : Comment agir quand on est victime de violences policières ? Comment agir quand on assiste à des violences policières ?
- 17h00-18h30 :  Projection du documentaire « Ici on noie les algériens » (2011)
- 18h45-19h45 :  Débat avec Olivier Le Cour Grandmaison, politologue enseignant à l’université Evry-Val d’Essonne, et Khaled Sid Mohand, journaliste indépendant.

Après l’événement, la cantine populaire du dimanche soir servira à manger pour tou-te-s à prix libre, à partir de 20h.
