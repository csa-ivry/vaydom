---
title: "Projection-discussion sur les résistance des femmes autochtones et allochtones contre des projets d’extraction au Québec"
date: 2018-02-01T10:55:36Z
draft: false
image: "2018/02/352d39e723cd7c525d9220da579733.jpg"
description: "Soirée de projection et de discussion des capsules vidéo réalisées par le collectif Des-terres-minées sur les résistances de femmes autochtones et allochtones contre des projets extractifs au « Québec » au CSA d’Ivry"
---

Soirée de projection et de discussion des capsules vidéo réalisées par le collectif Des-terres-minées sur les résistances de femmes autochtones et allochtones contre des projets extractifs au « Québec » au CSA d’Ivry

### Des-terres-minées !

Soirée de projection de capsules vidéo réalisées par le collectif Des-terres-minées ("Québec") sur la résistances de femmes autochtones et allochtones contre des projets extractifs au « Québec ». La soirée sera suivie d’une discussion en présence de l’une des membres du collectif.

Mardi 6 Février à 20h au CSA-Vaydom 37 rue Marceau, Ivry-sur-Seine

Tram 3a : Maryse Bastié ; Métro 7 :  Pierre et Marie Curie

**Entrée libre et gratuite !**

### Présentation du collectif Des-terres-minées :

Des-terres-minées est un projet de partages et de documentations des réalités vivantes. Notre collectif ouvre des espaces de parole et de réflexions collectives, à travers des perspectives féministes anti-coloniales, sur les thèmes du territoire et des enjeux extractifs (mines, hydrocarbures, exploitation forestière, hydroélectricité, etc.). Durant une tournée réalisée au "québec" au printemps 2016, nous avons récolté des témoignages de personnes allochtones et autochtones autour des leurs liens aux territoires, des menaces aux territoires, des impacts spécifiques de l’exploitation territoriale selon les genres et des résistances menées pour y faire face. 25 capsules-vidéo et 3 capsules-audio ont été réalisées à partir de ces paroles inspirantes.

Ces capsules sont un tremplin de discussions sur des questions de (néo)colonisation, d’extractivisme, de la place des femmes* comme toute personne vivant une oppression basée sur le genre dans les luttes contre la destruction du vivant…

[Site du collectif Des-terres-minées](http://desterresminees.pasc.ca)
