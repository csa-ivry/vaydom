---
title: "Assemblée des collectifs contre le Grand Paris, GPII et démolition des quartiers "
date: 2018-01-13T17:18:24Z
draft: false
image: "2018/01/dd021738c6088c911a1405120afb7d.jpg"
description: "Après-midi de rencontres, discussions et mise en place d’initiatives : samedi 13 janvier à partir de 14h au Centre Social Autogéré, 37 rue Marceau, Ivry sur Seine."
---

Après-midi de rencontres, discussions et mise en place d’initiatives : samedi 13 janvier à partir de 14h au Centre Social Autogéré, 37 rue Marceau, Ivry sur Seine.

Le 2 septembre dernier, de nombreux collectifs qui luttent contre des Grands Projets Inutiles Imposés et démolitions du Grand Paris se sont réunis et par la suite constitués en une « Assemblée des collectifs en lutte contre le Grand Paris ».

En effet , nous sommes nombreux à lutter sous différentes formes contre le Grand Paris et son projet de mégapole, avec ses conséquences comme la démolition de quartiers populaires et de logements sociaux, la marchandisation du patrimoine public, la spéculation immobilière et foncière, la crise du logement, les politiques d’urbanisme autoritaires entretenus et accélérés par les Jeux Olympiques, l’exposition universelle...

Tout cela se fait au profit des grands groupes, des promoteurs, des milieux du BTP et entraîne la hausse des prix des loyers et le logement cher, la disparition des terres agricoles, la dégradation de notre environnement, les expulsions et l’épuration sociale des quartiers populaires...

Ne nous laissons pas faire, organisons-nous ! Nous pouvons être plus forts, contre ces intérêts puissants. Pour mieux nous coordonner et élaborer des projets d’actions très larges, rencontrons-nous.

L’Assemblée organise une nouvelle réunion publique de rencontres, discussions et mise en place d’initiatives, où nous parlerons des luttes pour le logement et de l’ANRU, de la Butte Rouge à Chatenay Malabry, de la loi olympique et paralympique , de l’Exposition Universelle qui doit s’installer à Saclay et ailleurs. Nous réserverons aussi une large place pour échanger sur les luttes de nos collectifs respectifs et sur la manière de créer du commun entre elles.

Cette réunion aura lieu le samedi 13 janvier à partir de 14h au Centre Social Autogéré, 37 rue Marceau, Ivry sur Seine.

Assemblée des collectifs en lutte contre le Grand Paris
