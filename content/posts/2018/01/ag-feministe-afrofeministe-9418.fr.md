---
title: "AG Féministe/Afroféministe/Transféministe"
date: 2018-01-23T14:45:37Z
draft: false
image: "2018/01/47b405499ac6f55bfb3a995caa4606.jpg"
description: "AG (Assemblée Générale) Féministe/Afroféministe/Transféministe pour organiser la manifestation du 8 mars 2018."
---

AG (Assemblée Générale) Féministe/Afroféministe/Transféministe pour organiser la manifestation du 8 mars 2018.

![Photo de manif](/images/2018/01/26904277_956447771163115_3035592786402387216_n-b7467-ff400.jpg)

AG (Assemblée Générale) Féministe/Afroféministe/Transféministe pour organiser la manifestation du 8 mars 2018.

en mixité choisie (sans hommes cis*)

RDV le dimanche 28 janvier à 14h

Centre Social Autogéré Vaydom, 37 rue Marceau 94200 Ivry-sur-seine

Métro Pierre et Marie Curie ou Tram Maryse Bastié

Lieu accessible

*individu assigné garçon à la naissance et qui se définit toujours ainsi (comme « homme »)
