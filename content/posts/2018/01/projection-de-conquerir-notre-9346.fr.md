---
title: "Projection de « Conquérir notre autonomie »"
date: 2018-01-20T10:41:47Z
draft: false
image: "2018/01/9f70f4945d8953daf7908ce2df6d61.jpg"
description: "Ce documentaire critique sera visible pour la première fois en projection au CSA d’Ivry – 37 rue marceau – le 1er février à 19h suivi d’une période de questions/réponses."
---

Ce documentaire critique sera visible pour la première fois en projection au CSA d’Ivry – 37 rue marceau – le 1er février à 19h suivi d’une période de questions/réponses.

Depuis le mouvement contre la loi travail je produis des vidéos sur la philosophie, l’autonomie et l’anarchisme. 

Pendant presque un an j’ai préparé cette vidéo, écrit puis recommencé, pour créer un documentaire critique accessible à un grand nombre, mais aussi pertinent pour le milieux de l’autonomie politique. j’ai demandé de l’aide de militants, de philosophes, de scientifiques pour formaliser une critique radical, pouvant faire consensus et nous permettant d’avancer théoriquement et pratiquement.

J’y présente entre autre la critique de la valeur, la théorie critique Opéraiste et la théories de la communisation, pour arriver à l’action directe et l’organisation autonome sur des principes égalitaire et libertaire.

J’ai l’ambition que cette vidéo permette de partager la critique autonome à un plus grand nombre, dans toute sa rigueur et ses dimensions philosophiques, politiques et pratiques.

Ce documentaire critique sera visible pour la première fois en projection au CSA D’ivry – 36 rue marceau – le 1er février à 19h suivi d’une période de questions/réponses.

[Teaser](https://www.youtube.com/watch?v=uGo6z7oxTBg) de la vidéo :

{{< youtube "uGo6z7oxTBg" 512 60 >}}

Description de la vidéo :

On bétonne, la planète surchauffe, les écosystème sont exterminés. Nos conditions de vies sont de plus en plus fragiles, et tout espoir d’amélioration a disparu. On ne revendique plus le maintien de l’emploi mais des indemnités, on ne revendique rien mais on se révolte contre tout ce qui fait nos conditions d’existence. Quel sens peut avoir une grève corporatiste quand on sait que l’on aura 36 taffs différents dans une vie ? Alors comment construire la société de demain ?

Dans cet effondrement, ce documentaire critique présente les moyens théoriques et pratique de construire une société égalitaire et libertaire, de conquérir notre autonomie. Différentes pensées critique sont abordées pour construire une stratégie efficace, pour que le vivant se déploie face au système marchand dans sa totalité. Il nous faut maintenant réfléchir à ce que pourrait être une société sans travail – ce qui ne veut pas dire sans production, mais sans usines, sans chronomètres, sans souffrances.

[Chaîne Youtube](https://www.youtube.com/channel/UCzB4XvWgVlXFI4ljbQXzmwA)

Le documentaire est maintenant disponible sur Youtube : [Conquérir notre autonomie](https://www.youtube.com/watch?v=BXv4Txr9CYU)

{{< youtube "BXv4Txr9CYU" >}}
