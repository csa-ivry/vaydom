---
title: "Cap sur la pédagogie féministe !"
date: 2018-01-23T08:34:33Z
draft: false
image: "2018/01/9312f5857a98d3acfc1a0e16d083d5.png"
description: "C’est reparti pour un nouveau périple au pays des pédagogies émancipatrices ! Rejoins notre embarcation le temps d’une soirée ! Au CSA Vaydom d’Ivry le 25 janvier."
---

C’est reparti pour un nouveau périple au pays des pédagogies émancipatrices ! Rejoins notre embarcation le temps d’une soirée ! Au CSA Vaydom d’Ivry le 25 janvier.

![Affiche Cap sur la pédagogie féministe !](/images/2018/01/a_labordage_25_janv-0fc2f-a49d3.jpg)

Sur une mer agitée par les stéréotypes de genre et les rapports de domination, on sortira nos jumelles pour envisager de nouveaux horizons et on partira à la recherche d’outils pour changer de cap et arriver à tenir la barre !

18h / Enquête conscientisante
Discussion en binôme sur nos rapports à l’apprentissage, aux savoirs.

19h / Arpentage de textes sur la pédagogie féministe.

20h / Repas, à prix libre.

21h00 / Projection du film Espace d’Éléonor Gilbert et discussion sur les manières de prendre en compte les inégalités de genre dans nos pratiques éducatives.
