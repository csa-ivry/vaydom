---
title: "Procès en appel du CSA d’Ivry, deuxième épisode !"
date: 2018-01-28T12:06:39Z
draft: false
image: "2018/01/1c0b067d92d921a04aad376b05dc6d.png"
description: "Le 18 janvier nous étions assez nombreuseux au procès en appel du CSA, merci à toustes ceuxelles qui sont passé.es pour nous soutenir. Mais le procès a été rapporté au premier février, nous avons encore besoin de tout le monde pour lutter contre l’expulsion !"
---

Le 18 janvier nous étions assez nombreuseux au procès en appel du CSA, merci à toustes ceuxelles qui sont passé.es pour nous soutenir. Mais le procès a été rapporté au premier février, nous avons encore besoin de tout le monde pour lutter contre l’expulsion !

![Affiche Défendons le Vaydom](/images/2018/01/affiche_csa_vaydom_resiste_5_1_-7bded-1f1ae.png)

Jeudi 1 février à 14h, au palais de justice de Paris, se tiendra le procès en appel du CSA d’Ivry.

4 boulevard du Palais à Paris

Nous avions obtenu en première instance de pouvoir occuper les lieux jusqu’à avril 2019. Le Crédit Mutuel, propriétaire du bâtiment, a fait appel de cette décision et demande l’expulsion séance tenante des habitant-e-s et collectif du Vay Dom. Peu importe la vingtaine d’enfants qui vit ici. Peu importe les solidarités qui y naissent et y grandissent.

Lieu de solidarité, d’asile, de lutte, nous avons besoin que le Vaydom vive.

Nous invitons toutes celles et tous ceux qui aiment le CSA, qui trouvent important ce qu’on y vit et ce qu’on y fait ensemble, à se joindre à nous pour l’audience.

RDV pour un départ collectif le jeudi 18 janvier 2018 à 11H au CSA, 37 rue Marceau à Ivry (Métro Pierre et Marie Curie - ligne 7, station Maryse Bastié - tram 3a)

Vous pouvez aussi nous retrouver directement sur place à partir de 12h, au palais de justice de Paris, 4 boulevard du Palais (métro Cité - ligne 4). L’audience débutera à 14h mais l’accès à la salle d’audience peut être laborieux. Nous prévoyons donc de l’avance.

**Défendons le Vaydom !**
