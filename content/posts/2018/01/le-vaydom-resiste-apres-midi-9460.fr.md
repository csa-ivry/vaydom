---
title: "Le Vaydom résiste ! Après-midi solidaire contre l’expulsion"
date: 2018-01-28T15:39:22Z
draft: false
image: "2018/01/968136e5696ea2ef340d5a37e75b17.jpg"
description: "Un après-midi en soutien au CSA, contre son expulsion et pour continuer à tisser des solidarité autour de ce lieu.
Bouffe, boissons chaudes, musique et jeux sont assurés !"
---

Un après-midi en soutien au Csa, contre son expulsion et pour continuer à tisser des solidarité autour de ce lieu.
Bouffe, boissons chaudes, musique et jeux sont assurés !

Depuis plusieurs mois un nouveau lieu est occupé à Ivry pour y faire vivre un Centre Social Autogéré : le Vay Dom (qui signifie en tchétchène « notre maison »).
Laissé vide depuis quelques années, le bâtiment, appartenant au Crédit Mutuel et géré par Nexity, a été réquisitionné par des dizaines de mal logé.e.s, des familles, des précaires, des sans papiers.
Pour nous le squat est une réponse collective à la précarisation de nos vies, une manière de s’attaquer à la propriété immobilière, qui envoie de plus en plus de personnes à la rue, et un moyen de lutter collectivement contre la gentrification de nos quartiers.

Nous avions obtenu en première instance de pouvoir occuper les lieux jusqu’à avril 2019. Le Crédit Mutuel a fait appel de cette décision et demande l’expulsion séance tenante des habitant-e-s et collectif du Vay Dom. Peu importe la vingtaine d’enfants qui vit ici. Peu importe les solidarités qui y naissent et y grandissent.

Lieu de solidarité, d’asile, de lutte, nous avons tou.te.s besoin que le Vaydom vive.

Venez nous soutenir samedi 3 février, à partir de 15h, Place Voltaire à Ivry-sur-Seine (métro Mairie d’Ivry)

Bouffe, café thé bissap, gâteux, musique, infokiosque et bibliothèque, freeshop de fringues, jeux pour enfants et moins enfants, capoeira... soyons nombreu.ses.x !

PS : Nous avons aussi lancé un appel pour soutenir le Csa au procès, le premier février au palais de justice de Paris, 4 boulevard du Palais (métro Cité - ligne 4). L’audience débutera à 14h mais l’accès à la salle d’audience peut être laborieux. Nous prévoyons donc de l’avance.
