---
title: "Journée sur les luttes des prisonnier.es palestinien.ne.s au CSA d’Ivry"
date: 2018-01-10T10:57:07Z
draft: false
image: "2018/01/5363e1a8605e459a4e69f4a070fd09.jpg"
description: "Dimanche 21 janvier, à partir de 16h: Présentation, projection et débats autour de l’emprisonnement par l’État israélien des palestinien.nes, et des luttes des prisonnier.es contre la prison, le sionisme et l’impérialisme."
---

Dimanche 21 janvier, à partir de 16h, au Csa Vaydom :Présentation, projection et débats autour de l’emprisonnement par l’État israélien des palestinien.nes, et des luttes des prisonnier.es contre la prison, le sionisme et l’impérialisme.

Programme :

**16h** : Présentation du livre Des hommes entre les murs, de Assia Zaino.
Le livre recueille des entretiens de militant.es palestinien.nes du village de Nabi Saleh (West Bank), qui ont dû faire face à l’emprisonnement ainsi qu’aux violences de l’armée israélienne et des colons sionistes.

**18h** : Présentation du Festival Ciné-Palestine, et à suivre
Projection du film 3000 Nuits de Mai Masri : l’histoire d’une prisonnière politique palestinienne.

**20h** : Cantine populaire à prix libre et végan

Pour plus d’info sur le festival Ciné-Palestine: http://festivalpalestine.paris/
