---
title: "Les luttes des  migrant.es travailleurs-ses agricoles en Italie : Rencontre avec le collectif Campagne in Lotta"
date: 2018-01-25T10:25:21Z
draft: false
image: "2018/01/55a38673b8283ee791fda6301c9e2d.jpg"
description: "Le Csa Vaydom invite le collectif italien Campagne In Lotta pour discuter de l’exploitation subie par les travailleurs et travailleuses migrant.es dans les campagnes italiennes…"
---

Le Csa Vaydom invite le collectif italien Campagne In Lotta pour discuter de l’exploitation subie par les travailleurs et travailleuses migrant.es dans les campagnes italiennes, de ses liens avec les politiques migratoires, et des luttes que ces dernier-es mènent depuis des années, contre les patrons et la filière agro-industrielle, contre le traitement humanitaro-policier de l’État, pour les papiers et le logement pour tout le monde !

![Manif de migrant-e-s](/images/2018/01/retecampagneinlotta-12cca-b2d0f.jpg)

> Le réseau Campagne in Lotta (« Campagnes en lutte ») est né avec l’objectif de mettre en lien travailleurs et travailleuses -notamment étrangèrEs- présent-e-s sur différents territoires de la production agroalimentaire italienne, avec des individus et des collectifs militants.
>
> Créer des réseaux pour se rencontrer et se coordonner, afin de construire ensemble des processus d’auto-determination et d’auto-organisation, pour investir différents domaines de la lutte.
>
> Notre expérience commence en août 2011 avec la rencontre entre deux réalités : d’un coté l’assemblée des Travailleurs Africains de Rosarno (ALAR), crée à Rome suite à la Révolte de Rosarno de janvier 2010, et de l’autre les travailleurs et les personnes solidaires avec la grève de Nardo’ d’août 2011. Les luttes de Rosarno et Nardo’, comme d’autres avant elles, ont fait émerger les conditions de travail et de vie extrêmement dures subies par les travailleurs/euses des campagnes, mais aussi leur envie de lutter.
>
> Le réseau s’est donc constitué en tant qu’outil pour rompre leur isolement, soutenir leurs revendications autodéterminées, et amener des pratiques de solidarité concrètes à différents niveaux : juridique, d’échange de connaissances et informations, de sociabilité et des discussions politiques. Aujourd’hui le réseau de Campagne In Lotta est composé de travailleurs/euses précaires étrangerEs et italienNEs, chômeurs/euses, individuels, ou organisés dans des collectifs.

[Présentation](http://campagneinlotta.org/chi-siamo/?lang=fr) tirée du site du collectif. Plus d’info et de matériel informatif, en français aussi, ici http://campagneinlotta.org/?lang=fr

