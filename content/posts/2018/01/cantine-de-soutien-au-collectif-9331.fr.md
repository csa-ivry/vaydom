---
title: "Cantine de soutien au collectif Baras au Csa Vaydom"
date: 2018-01-10T08:06:54Z
draft: false
image: "2018/01/f4967755b8b01674106decaa800491.jpg"
description: "Dimanche 14 janvier, maffé et tiep (végétariens et non) à la cantine du Csa Vaydom, en soutien à la lutte du collectif des Baras ! A 19h, présentation du collectif et discussion, A 20h, cantine à prix libre"
---

Dimanche 14 janvier, maffé et tiep (végétariens et non) à la cantine du Csa Vaydom, en soutien à la lutte du collectif des Baras ! A 19h, présentation du collectif et discussion, A 20h, cantine à prix libre

Le collectif Baras est un collectif de sans-papiers qui luttent depuis 2012 pour la régularisation collective et pour le logement. Nous avons
squatté plusieurs bâtiments, qui ont toujours été expulsés. La dernière expulsion a été en juin 2017, suite à laquelle nous nous sommes retrouvés à la rue. En août 2017 nous avons occupé un bâtiment où ils vivent actuellement, au 72 rue des Bruyères aux Lilas.

![Bâtiment des Baras](/images/2018/01/collectif-baras-98e9e-5c3a2.jpg)

Vous êtes toustes invité.es au Csa Vaydom à 19h (présentation du collectif) et pour un bon repas de tiep et maffé dimanche soir à partir de 20h
et n’hésitez pas à passer nous rendre visite aux Lilas pour nous soutenir !

Le collectif Baras

Plus d’info sur le collectif Baras [ici](https://fr.squat.net/tag/collectif-baras/).

Contact : lesbaras@squat.net
